export interface ClienteResponse {
    current_page:   number;
    data:           Cliente[];
    first_page_url: string;
    from:           number;
    last_page:      number;
    last_page_url:  string;
    links:          Link[];
    next_page_url:  null;
    path:           string;
    per_page:       string;
    prev_page_url:  null;
    to:             number;
    total:          number;
}

export interface Cliente {
    phone:             string;
    phone_contact:     string | null;
    residence_condition: string | null;
    status: string | null;
    full_name_contact: string | null;
    client_type_id:    number | null;
    document_type_id:  number;
    document_number:   string;
    id:                number;
    name:              string;
    last_name:         string;
    email:             string;
    email_verified_at: Date | null;
    user_created_id:   number;
    user_updated_id:   number | null;
    created_at:        Date | null;
    updated_at:        Date | null;
    deleted_at:        Date | null;
    client_type:       Metadata | null;
    document_type:     Metadata | null;
}

export interface Metadata {
    id:              number;
    name:            Name;
    user_created_id: string;
    user_updated_id: null;
    created_at:      Date | null;
    updated_at:      Date | null;
    deleted_at:      null;
}

export enum Name {
    Distribuidor = "Distribuidor",
    Dni = "DNI",
    Interno = "Interno",
    Ruc = "RUC",
}

export interface Link {
    url:    null | string;
    label:  string;
    active: boolean;
}
export class ClienteModel {
    static clienteFromJson(obj:any) {
        return new ClienteModel(
            obj['id'],
            obj['name'],
            obj['last_name'],
            obj['email'],
            obj['document_number'],
            obj['document_type'],
            obj['client_type'],
            obj['phone'],
            obj['phone_contact']
        );
    }
    constructor(
        public id:                number,
        public name:              string,
        public last_name:         string,
        public email:             string,
        public document_number:   string,
        public document_type:     Metadata | null,
        public client_type:       Metadata | null,
        public phone:             string,
        public phone_contact:     string,
    ) {}
    get documentType() {
        if (this.document_type) {
            return this.document_type.name
        } else {
            return
        }
    }
    get fullName() {
        if (this.last_name == 'N/A') {
            return this.name
        } else {
            return `${this.name} ${this.last_name}`
        }
    }
    get clientType() {
        if (this.client_type) {
            return this.client_type.name
        } else {
            return
        }
    }
}