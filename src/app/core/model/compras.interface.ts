export interface ComprasResponse {
    current_page:   number;
    data:           Compra[];
    first_page_url: string;
    from:           number;
    last_page:      number;
    last_page_url:  string;
    links:          Link[];
    next_page_url:  null;
    path:           string;
    per_page:       string;
    prev_page_url:  null;
    to:             number;
    total:          number;
}

export interface Compra {
    id:                number;
    serie:             string;
    number:            string;
    voucher_type_id:   number;
    operation_type_id: number;
    provider_id:       number;
    coin_id:           number;
    exchange_rate:     string;
    igv:               string;
    expiration_date:   Date;
    user_created_id:   number;
    user_updated_id:   null;
    created_at:        Date;
    updated_at:        Date;
    deleted_at:        null;
    coin:              Coin;
    provider:          Provider;
    voucher_type:      Coin;
    purchase_payments: PurchasePayment[];
    purchase_details:  PurchaseDetail[];
}

export interface Coin {
    id:   number;
    name: string;
}

export interface Provider {
    id:            number;
    name:          string;
    last_name:     string;
    document_type: null;
}

export interface PurchaseDetail {
    id:               number;
    purchase_id:      number;
    product_id:       number;
    branch_office_id: number;
    amount:           number;
    sale_price:       string;
    purchase_price:   string;
    discount:         string;
    igv:              string;
    user_created_id:  number;
    user_updated_id:  null;
    created_at:       Date;
    updated_at:       Date;
    deleted_at:       null;
    product:          Product;
}

export interface Product {
    id:              number;
    name:            string;
    stock:           Stock[];
    category:        null;
    brand:           null;
    attribute_types: AttributeType[];
}

export interface AttributeType {
    id:              number;
    name:            string;
    description:     string;
    user_created_id: number;
    user_updated_id: null;
    created_at:      null;
    updated_at:      null;
    deleted_at:      null;
    pivot:           Pivot;
}

export interface Pivot {
    product_id:        number;
    attribute_type_id: number;
    description:       string;
}

export interface Stock {
    branch_office_id:   number;
    branch_office_name: string;
    purchase_price:     string;
    sale_price:         string;
    stock_product:      number;
}

export interface PurchasePayment {
    id:                     number;
    purchase_id:            number;
    payment_method_id:      number;
    payment_destination_id: number;
    reference:              string;
    amount:                 string;
    user_created_id:        number;
    user_updated_id:        null;
    created_at:             Date;
    updated_at:             Date;
    deleted_at:             null;
}

export interface Link {
    url:    null | string;
    label:  string;
    active: boolean;
}
export class CompraModel {
    static comprasFromJson(obj:any) {
        return new CompraModel(
            obj['id'],
            obj['serie'],
            obj['number'],
            obj['voucher_type_id'],
            obj['operation_type_id'],
            obj['provider_id'],
            obj['coin_id'],
            obj['exchange_rate'],
            obj['igv'],
            obj['expiration_date'],
            obj['user_created_id'],
            obj['user_updated_id'],
            obj['created_at'],
            obj['updated_at'],
            obj['deleted_at'],
            obj['coin'],
            obj['provider'],
            obj['voucher_type'],
            obj['purchase_payments'],
            obj['purchase_details'],
        );
    }
    constructor(
        public id:                number,
        public serie:             string,
        public number:            string,
        public voucher_type_id:   number,
        public operation_type_id: number,
        public provider_id:       number,
        public coin_id:           number,
        public exchange_rate:     string,
        public igv:               string,
        public expiration_date:   Date,
        public user_created_id:   number,
        public user_updated_id:   null,
        public created_at:        Date,
        public updated_at:        Date,
        public deleted_at:        null,
        public coin:              Coin,
        public provider:          Provider,
        public voucher_type:      Coin,
        public purchase_payments: PurchasePayment[],
        public purchase_details:  PurchaseDetail[],
    ) {

    }
    get createdAt() {
        return new Date(this.created_at).toLocaleString()
    }
}