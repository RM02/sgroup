export interface TransferResponse {
    current_page:   number;
    data:           Transfer[];
    first_page_url: string;
    from:           number;
    last_page:      number;
    last_page_url:  string;
    links:          Link[];
    next_page_url:  null;
    path:           string;
    per_page:       string;
    prev_page_url:  null;
    to:             number;
    total:          number;
}

export interface Transfer {
    id:                number;
    from_warehouse_id: number;
    to_warehouse_id:   number;
    user_created_id:   number;
    user_updated_id:   number | null;
    created_at:        Date | null;
    updated_at:        Date | null;
    deleted_at:        Date | null;
    transfer_details:  TransferDetail[];
    to_warehouse:      Warehouse;
    from_warehouse:    Warehouse;
}

export interface Warehouse {
    id:          number;
    description: string;
}

export interface TransferDetail {
    id:              number;
    transfer_id:     number;
    product_id:      number;
    amount:          number;
    user_created_id: number;
    user_updated_id: null;
    created_at:      Date;
    updated_at:      Date;
    deleted_at:      null;
    product:         Product;
}

export interface Product {
    id:   number;
    name: string;
}

export interface Link {
    url:    null | string;
    label:  string;
    active: boolean;
}
export class TransferModel {
    static transferFromJson(obj:any) {
        return new TransferModel(
            obj[`id`],
            obj[`from_warehouse_id`],
            obj[`to_warehouse_id`],
            obj[`user_created_id`],
            obj[`user_updated_id`],
            obj[`created_at`],
            obj[`updated_at`],
            obj[`deleted_at`],
            obj[`transfer_details`],
            obj[`to_warehouse`],
            obj[`from_warehouse`],
        )
    }
    constructor(
        public id:                number,
        public from_warehouse_id: number,
        public to_warehouse_id:   number,
        public user_created_id:   number,
        public user_updated_id:   number | null,
        public created_at:        Date | null,
        public updated_at:        Date | null,
        public deleted_at:        Date | null,
        public transfer_details:  TransferDetail[],
        public to_warehouse:      Warehouse,
        public from_warehouse:    Warehouse,

    ) {}
    get origen() {
        if (this.from_warehouse && this.from_warehouse.description) {
            return this.from_warehouse.description
        } else {
            return
        }
    }
    get destino() {
        if (this.to_warehouse && this.to_warehouse.description) {
            return this.to_warehouse.description
        } else {
            return
        }
    }
    get total() {
        let total = 0
        this.transfer_details.map((item:any) => {
            total += item.amount
        })
        return total
    }
}
