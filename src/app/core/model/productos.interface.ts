
  export interface ProductosResponse {
    current_page:   number;
    data:           ProductosModel[];
    first_page_url: string;
    from:           number;
    last_page:      number;
    last_page_url:  string;
    links:          Link[];
    next_page_url:  string;
    path:           string;
    per_page:       string;
    prev_page_url:  null;
    to:             number;
    total:          number;
}

export interface Productos {
    id:              number;
    code:            string;
    name:            string;
    supsec:          string;
    numsec:          string;
    description:     string;
    category_id:     number;
    brand_id:        number;
    user_created_id: number;
    user_updated_id: number | null;
    created_at:      Date;
    updated_at:      Date;
    deleted_at:      null;
    stock:           Stock[];
    category:        Atributo;
    brand:           Atributo;
    attribute_types: Atributo[];
}

interface Atributo {
    id:              number;
    name:            string;
    description:     string;
    user_created_id: number;
    user_updated_id: null;
    created_at:      Date | null;
    updated_at:      Date | null;
    deleted_at:      null;
    pivot?:          Pivot;
}

interface Pivot {
    product_id:        number;
    attribute_type_id: number;
    description:       string;
}

export interface Stock {
    branch_office_id:   number;
    purchase_price:     string;
    sale_price:         string;
    stock_product:      number;
    warehouse_id:       number;
    warehouse_name:     string;
}

interface Link {
    url:    null | string;
    label:  string;
    active: boolean;
}

export class ProductosModel {
  static productosFromJson(obj: any) {
    return new ProductosModel(
      obj['id'],
      obj['code'],
      obj['name'],
      obj['supsec'],
      obj['numsec'],
      obj['description'],
      obj['category_id'],
      obj['brand_id'],
      obj['user_created_id'],
      obj['user_updated_id'],
      obj['created_at'],
      obj['updated_at'],
      obj['deleted_at'],
      obj['stock'],
      obj['category']['name'],
      obj['brand']['name'],
      obj['attribute_types']     
    );
  }
  constructor(
    public  id:              number,
    public  code:            string,
    public  name:            string,
    public  supsec:          string,
    public  numsec:          string,
    public  description:     string,
    public  category_id:     number,
    public  brand_id:        number,
    public  user_created_id: number,
    public  user_updated_id: number | null,
    public  created_at:      Date,
    public  updated_at:      Date,
    public  deleted_at:      Date | null,
    public  stock:           Stock[],
    public  category:        Atributo,
    public  brand:           Atributo,
    public  attribute_types: Atributo[],
  ) {}
  get modelo() {
    let modelo = this.attribute_types.filter((item) => item.id == 1)
    if (modelo.length > 0 && modelo[0].pivot) {
      return modelo[0]['pivot']['description']
    } else {
      return 
    }
  }
  get um() {
    let um = this.attribute_types.filter((item) => item.id == 10)
    if (um.length > 0 && um[0].pivot) {
      return um[0]['pivot']['description']
    } else {
      return 
    }
  }
  get cmotor() {
    let cmotor = this.attribute_types.filter((item) => item.id == 2)
    if (cmotor.length > 0 && cmotor[0].pivot) {
      return cmotor[0]['pivot']['description']
    } else {
      return
    }
  }
  get alternante() {
    let alternante = this.attribute_types.filter((item) => item.id == 9)
    if (alternante.length > 0 && alternante[0].pivot) {
      return alternante[0]['pivot']['description']
    } else {
      return
    }
  }
  get cilindros() {
    let cilindros = this.attribute_types.filter((item) => item.id == 5)
    if (cilindros.length > 0 && cilindros[0].pivot) {
      return cilindros[0]['pivot']['description']
    } else {
      return
    }
  }
  get diametro() {
    let diametro = this.attribute_types.filter((item) => item.id == 6)
    if (diametro.length > 0 && diametro[0].pivot) {
      return diametro[0]['pivot']['description']
    } else {
      return
    }
  }
  get numcil() {
    let numcil = this.attribute_types.filter((item) => item.id == 3)
    if (numcil.length > 0 && numcil[0].pivot) {
      return numcil[0]['pivot']['description']
    } else {
      return
    }
  }
  get almacen() {
    if (this.stock && this.stock.length > 0) {
      return this.stock[0].warehouse_name
    } else {
      return
    }
  }
  get stock_value() {
    if (this.stock && this.stock.length > 0) {
      return this.stock[0].stock_product
    } else {
      return
    }
  }
  get from_warehouse_id() {
    if(this.stock && this.stock.length > 0) {
      return this.stock[0].warehouse_id
    } else {
      return
    }
  }
}