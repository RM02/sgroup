export interface LoginResponse {
    token_type:    string;
    expires_in:    number;
    access_token:  string;
    refresh_token: string;
    user:          User;
}

export interface User {
    phone:             null;
    phone_contact:     null;
    full_name_contact: null;
    client_type_id:    null;
    document_type_id:  null;
    document_number:   null;
    id:                number;
    name:              string;
    last_name:         string;
    email:             string;
    email_verified_at: Date;
    user_created_id:   number;
    user_updated_id:   number;
    created_at:        Date;
    updated_at:        Date;
    deleted_at:        null;
    roles:             Role[];
    branch_offices:    BranchOffice[];
}

export interface BranchOffice {
    id:              number;
    name:            string;
    acronym:         string;
    user_created_id: number;
    user_updated_id: null;
    created_at:      null;
    updated_at:      null;
    deleted_at:      null;
    pivot:           BranchOfficePivot;
}

export interface BranchOfficePivot {
    user_id:          number;
    branch_office_id: number;
}

export interface Role {
    id:              number;
    name:            string;
    acronym:         string;
    user_created_id: number;
    user_updated_id: null;
    created_at:      null;
    updated_at:      null;
    deleted_at:      null;
    pivot:           RolePivot;
}

export interface RolePivot {
    user_id: number;
    role_id: number;
}
