export interface ReporteKardexResponse {
  current_page:   number;
  data:           ReporteKardex[];
  first_page_url: string;
  from:           number;
  last_page:      number;
  last_page_url:  string;
  links:          Link[];
  next_page_url:  null;
  path:           string;
  per_page:       string;
  prev_page_url:  null;
  to:             number;
  total:          number;
}

export interface ReporteKardex {
  id:              number;
  reportable_type: string;
  reportable_id:   number;
  stock:           any[];
  product_id:      number;
  user_created_id: number;
  user_updated_id: null;
  created_at:      Date;
  updated_at:      Date;
  deleted_at:      null;
  reportable:      Reportable;
  product:         Product;
}

export interface Product {
  id:   number;
  name: string;
}

export interface Reportable {
  id:                 number;
  bill_electronic_id: number;
  product_id:         number;
  amount:             number;
  price:              number;
  discount:           number;
  igv:                number;
  purchase_price:     number;
  user_created_id:    number | null;
  user_updated_id:    number | null;
  created_at:         Date | null ;
  updated_at:         Date | null;
  deleted_at:         Date | null;
}

export interface Link {
  url:    null | string;
  label:  string;
  active: boolean;
}
export class ReporteKardexModel {
    static reporteFromJson(obj:any) {
      return new ReporteKardexModel(
        obj['id'],
        obj['entrada'],
        obj['salida'],
        obj['reportable_type'],
        obj['reportable_id'],
        obj['stock'],
        obj['product_id'],
        obj['user_created_id'],
        obj['user_updated_id'],
        obj['created_at'],
        obj['updated_at'],
        obj['deleted_at'],
        obj['reportable'],
        obj['product'],
      )
    }
    constructor(
      public id:              number,
      public entrada: number | null,
      public salida: number | null,
      public reportable_type: string | undefined,
      public reportable_id:   number,
      public stock:           any[],
      public product_id:      number,
      public user_created_id: number,
      public user_updated_id: null,
      public created_at:      Date,
      public updated_at:      Date,
      public deleted_at:      null,
      public reportable:      Reportable,
      public product:         Product,
  ) {}
  get reportableType() {
    let type = null
    switch (this.reportable_type) {
      case "App\\Models\\BillElectronicDetail":
        type = 'Venta'
        return type
        break;
      case "App\\Models\\DevolutionDetail":
        type = 'Devolución'
        return type
        break;
      case "App\\Models\\TransferDetail":
        type = 'Traslado'
        return type  
      default:
        type = '-'
        return type
        break;
    }
  }
  get entradaValue() {
    if (this.reportable.price && this.reportable_type == 'App\\Models\\DevolutionDetail') {
      this.entrada = this.reportable.price * this.reportable.amount
      return this.entrada
    } else {
      return `${'-'}`
    }
  }
  get salidaValue() {
    if (this.reportable_type == 'App\\Models\\BillElectronicDetail') {
      this.salida = this.reportable.price * this.reportable.amount
      return this.salida
    } else {
      return `${'-'}`
    }    
  }
}
