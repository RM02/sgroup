export enum Role {
    Super_Admin = 1,
    Admin = 2,
    Vendedor = 3,
    Logístico = 8,
    Tesorería = 9,
    Cajera = 10,
    Almacen = 11,
    Contabilidad = 12
}