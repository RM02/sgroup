export interface ComprobantesResponse {
    current_page:   number;
    data:           Comprobantes[];
    first_page_url: string;
    from:           number;
    last_page:      number;
    last_page_url:  string;
    links:          Link[];
    next_page_url:  null;
    path:           string;
    per_page:       string;
    prev_page_url:  null;
    to:             number;
    total:          number;
}

export interface Comprobantes {
    id:                       number;
    serie_id:                 number;
    client_id:                number;
    voucher_type_id:          number;
    branch_office_id:         number;
    operation_type_id:        number;
    seller_id:                number;
    coin_id:                  number;
    exchange_rate:            string;
    igv:                      string;
    expiration_date:          Date;
    user_created_id:          number;
    user_updated_id:          null;
    created_at:               Date;
    updated_at:               Date;
    deleted_at:               null;
    total:                    number;
    total_igv:                number;
    total_gravado:            number;
    coin:                     Coin;
    serie:                    Coin;
    client:                   Client;
    seller:                   Seller;
    voucher_type:             Coin;
    bill_electronic_details:  BillElectronicDetail[];
    bill_electronic_payments: BillElectronicPayment[];
    bill_electronic_guides:   any[];
}

export interface BillElectronicDetail {
    id:                 number;
    bill_electronic_id: number;
    product_id:         number;
    amount:             number;
    price:              string;
    discount:           string;
    igv:                string;
    purchase_price:     string;
    user_created_id:    number;
    user_updated_id:    null;
    created_at:         Date;
    updated_at:         Date;
    deleted_at:         null;
    product:            Product;
}

export interface Product {
    id:              number;
    name:            string;
    stock:           Stock[];
    category:        null;
    brand:           null;
    attribute_types: AttributeType[];
}

export interface AttributeType {
    id:              number;
    name:            string;
    description:     string;
    user_created_id: number;
    user_updated_id: null;
    created_at:      null;
    updated_at:      null;
    deleted_at:      null;
    pivot:           Pivot;
}

export interface Pivot {
    product_id:        number;
    attribute_type_id: number;
    description:       string;
}

export interface Stock {
    branch_office_id:   number;
    branch_office_name: string;
    purchase_price:     string;
    sale_price:         string;
    stock_product:      number;
}

export interface BillElectronicPayment {
    id:                     number;
    bill_electronic_id:     number;
    payment_method_id:      number;
    payment_destination_id: number;
    reference:              null | string;
    amount:                 string;
    user_created_id:        number;
    user_updated_id:        null;
    created_at:             Date;
    updated_at:             Date;
    deleted_at:             null;
    payment_method:         Coin;
    payment_destination:    Coin;
}

export interface Coin {
    id:   number;
    name: string;
}

export interface Client {
    id:            number;
    name:          string;
    last_name:     string;
    client_type:   null;
    document_type: null;
}

export interface Seller {
    id:        number;
    name:      string;
    last_name: string;
}

export interface Link {
    url:    null | string;
    label:  string;
    active: boolean;
}


export class ComprobanteModel {
    static comprobanteFromJson(obj: any) {
      return new ComprobanteModel(
        obj['id'],
        obj['serie_id'],     
        obj['client_id'],     
        obj['voucher_type_id'],     
        obj['branch_office_id'],     
        obj['operation_type_id'],     
        obj['seller_id'],     
        obj['coin_id'],     
        obj['exchange_rate'],     
        obj['igv'],     
        obj['expiration_date'],   
        obj['user_created_id'],     
        obj['user_updated_id'],   
        obj['created_at'],   
        obj['updated_at'],   
        obj['deleted_at'],
        obj['total'],
        obj['total_igv'],
        obj['total_gravado'],
        obj['coin'],
        obj['serie'],
        obj['client'],
        obj['seller'],
        obj['voucher_type'],
        obj['bill_electronic_details'],
        obj['bill_electronic_payments'],
        obj['bill_electronic_guides'],
      );
    }
    constructor(
        public id:                       number,
        public serie_id:                 number,
        public client_id:                number,
        public voucher_type_id:          number,
        public branch_office_id:         number,
        public operation_type_id:        number,
        public seller_id:                number,
        public coin_id:                  number,
        public exchange_rate:            string,
        public igv:                      string,
        public expiration_date:          Date,
        public user_created_id:          number,
        public user_updated_id:          null,
        public created_at:               Date,
        public updated_at:               Date,
        public deleted_at:               null,
        public total:                    number,
        public total_igv:                number,
        public total_gravado:            number,
        public coin:                     Coin,
        public serie:                    Coin,
        public client:                   Client,
        public seller:                   Seller,
        public voucher_type:             Coin,
        public bill_electronic_details:  BillElectronicDetail[],
        public bill_electronic_payments: BillElectronicPayment[],
        public bill_electronic_guides:   any[],
    ) {
    
    }
    get number() {
        return this.serie.name ? `${this.serie.name}-${this.id}`: ''
    }
    get voucher() {
        if (this.voucher_type.name) {
            return this.voucher_type.name
        } else {
            return
        }
    }
    get cliente() {
        if (this.client.name) {
            return this.client.name
        }
        else {
            return
        }
    }
    get coinName() {
        if (this.coin && this.coin.name) {
            return this.coin.name
        }
        else {
            return
        }
    }
    get formatCreatedAt() {
        let date = new Date(this.created_at).toLocaleString()
        return date
    }
    get formatExpirationDate() {
        let date = new Date(this.expiration_date).toLocaleString()
        return date
    }
  }