export interface WarehouseResponse {
    current_page:   number;
    data:           Warehouses[];
    first_page_url: string;
    from:           number;
    last_page:      number;
    last_page_url:  string;
    links:          Link[];
    next_page_url:  null;
    path:           string;
    per_page:       string;
    prev_page_url:  null;
    to:             number;
    total:          number;
}

export interface Warehouses {
    id:               number;
    description:      string;
    branch_office_id: number;
    user_created_id:  number;
    user_updated_id:  number | null;
    created_at:       Date | null;
    updated_at:       Date | null;
    deleted_at:       Date | null;
    branch_office:    BranchOffice;
}

export interface BranchOffice {
    id:   number;
    name: string;
}

export interface Link {
    url:    null | string;
    label:  string;
    active: boolean;
}
export class WarehouseModel {
    static warehouseFromJson(obj:any) {
      return new WarehouseModel(
        obj['id'],
        obj['description'],
        obj['branch_office_id'],
        obj['user_created_id'],
        obj['user_updated_id'],
        obj['created_at'],
        obj['updated_at'],
        obj['deleted_at'],
        obj['branch_office'],
      )
    }
    constructor(
        public id:               number,
        public description:      string,
        public branch_office_id: number,
        public user_created_id:  number,
        public user_updated_id:  number | null,
        public created_at:       Date | null,
        public updated_at:       Date | null,
        public deleted_at:       Date | null,
        public branch_office:    BranchOffice,
        ) {}
    
  get branchOffice() {
    if(this.branch_office) {
      return this.branch_office.name
    } else {
      return null
    }
  }
}
