export interface ProviderResponse {
    current_page:   number;
    data:           Providers[];
    first_page_url: string;
    from:           number;
    last_page:      number;
    last_page_url:  string;
    links:          Link[];
    next_page_url:  null;
    path:           string;
    per_page:       string;
    prev_page_url:  null;
    to:             number;
    total:          number;
}

export interface Providers {
    phone:               null;
    residence_condition: null;
    status:              null;
    phone_contact:       null;
    full_name_contact:   null;
    document_type_id:    null;
    document_number:     null;
    id:                  number;
    name:                string;
    last_name:           string;
    email:               string;
    email_verified_at:   Date;
    user_created_id:     number;
    user_updated_id:     null;
    created_at:          Date;
    updated_at:          Date;
    deleted_at:          null;
    document_type:       Metadata;
}
interface Metadata {
    id: number;
    name: string;
    description: string;
}

export interface Link {
    url:    null | string;
    label:  string;
    active: boolean;
}
export class ProviderModel {
    static providersFromJson(obj:any) {
        return new ProviderModel(
            obj['phone'],
            obj['residence_condition'],
            obj['status'],
            obj['phone_contact'],
            obj['full_name_contact'],
            obj['document_type_id'],
            obj['document_number'],
            obj['id'],
            obj['name'],
            obj['last_name'],
            obj['email'],
            obj['email_verified_at'],
            obj['user_created_id'],
            obj['user_updated_id'],
            obj['created_at'],
            obj['updated_at'],
            obj['deleted_at'],
            obj['document_type'],
        );
    }
    constructor(
        public phone:               number,
        public residence_condition: string,
        public status:              string,
        public phone_contact:       null,
        public full_name_contact:   null,
        public document_type_id:    number,
        public document_number:     number,
        public id:                  number,
        public name:                string,
        public last_name:           string,
        public email:               string,
        public email_verified_at:   Date,
        public user_created_id:     number,
        public user_updated_id:     null,
        public created_at:          Date,
        public updated_at:          Date,
        public deleted_at:          null,
        public document_type:       Metadata,      
    ) {}
    get fullName() {
        if(this.document_type.name == 'RUC') {
            return this.name
        } else {
            return `${this.name} ${this.last_name}`
        }
    }
}
