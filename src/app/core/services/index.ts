import { ClientService } from './client.services';
import { ComprobanteService } from './comprobantes.services';
import { SellersService } from './seller.services';
import { ProductoService } from './productos.service';
import { OperationTypesService } from './operation-type.services';
import { CoinService } from './coin.services';
import { VoucherTypeService } from './voucher-type.services';
import { ExternalService } from './external.services';
import { PaymentService } from './payment.services';
import { CategoriasService } from './categorias.services';
import { MarcasService } from './marcas.services';
import { ClientTypeService } from './clientType.services';
import { DocumentTypeService } from './documentType.services';
import { AuthenticationService } from './auth.services';
import { AtributoTypeService } from './atributoType.services';
import { PedidoService } from './pedido.services';
import { ComisionesProductosService } from './comisiones-productos.services';
import { ComisionesVendedoresService } from './comisiones-vendedores.services';
import { SeriesService } from './serie.services';
import { CotizacionesService } from './cotizaciones.services';
import { GeneratorService } from './generator.service';
import { GuiasService } from './guias.service';
import { CreditNoteTypesService } from './tipo-nota-credito.services';
import { ComprasService } from './compras.services';
import { ProvidersService } from './providers.services';
import { OfficesService } from './branch-office.services';
import { UsersService } from './users.services';
import { RolesService } from './roles.services';
import { WarehouseService } from './warehouses.services';
import { ReporteKardexService } from './reporte-kardex.services';
import { DevolucionesService } from './devoluciones.services';
import { DevolucionReasonService } from './reaso-devolucion.services';
import { TransfersService } from './transfers.services';
import { OrdenesComprasService } from './ordenes-compra.services';

export {
    OrdenesComprasService,
    ClientService,
    ComprobanteService,
    SellersService,
    ProductoService,
    OperationTypesService,
    CoinService,
    VoucherTypeService,
    ExternalService,
    PaymentService,
    CategoriasService,
    MarcasService,
    ClientTypeService,
    DocumentTypeService,
    AuthenticationService,
    AtributoTypeService,
    PedidoService,
    ComisionesProductosService,
    ComisionesVendedoresService,
    SeriesService,
    CotizacionesService,
    GeneratorService,
    GuiasService,
    CreditNoteTypesService,
    ComprasService,
    ProvidersService,
    OfficesService,
    UsersService,
    RolesService,
    WarehouseService,
    ReporteKardexService,
    DevolucionesService,
    DevolucionReasonService,
    TransfersService
}