import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class OperationTypesService {
        
    constructor(private http: HttpClient) { }

    getAll() {

        return this.http.get(`${environment.apiUrl}/operation-types`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('sortOrder', 'desc')
            .set('sortField', 'id')
            .set('perPage', '20')
        })
    }

}