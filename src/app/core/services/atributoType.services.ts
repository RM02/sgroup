import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AtributoTypeService {
        
    constructor(private http: HttpClient) { }

    getAttributes() {

        return this.http.get(`${environment.apiUrl}/attribute-types`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('sortOrder', 'desc')
            .set('sortField', 'id')
            .set('perPage', '20')
        })
    }

}