import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ComisionesProductosService {
        
    constructor(private http: HttpClient) { }

    getAll(params?:any) {

        return this.http.get(`${environment.apiUrl}/product-commissions`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('perPage', '20')
            .set('sortField', 'id')
            .set('sortOrder', 'desc')
            .set('dataFilter', params ? JSON.stringify(params): '')
        })
    }
    post(data:any) {
        return this.http.post(`${environment.apiUrl}/product-commissions`, data)
    }
    update(id:any, data:any) {
        return this.http.put(`${environment.apiUrl}/product-commissions/${id}`, data)
    }

    delete(id:any) {
        return this.http.delete(`${environment.apiUrl}/product-commissions/${id}`)
    }
}