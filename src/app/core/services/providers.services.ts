import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ProviderModel } from '../model/providers.interface';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ProvidersService {
        
    constructor(private http: HttpClient) { }

    getProviders(params ?: any) {

        return this.http.get(`${environment.apiUrl}/providers`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('perPage', '20')
            .set('sortOrder', 'desc')
            .set('sortField', 'id')
            .set('dataSearch', params ? JSON.stringify(params): '')
        })
        .pipe(
            map((res:any) => {
                return res['data'].map((pro:any) => ProviderModel.providersFromJson(pro))
            })
        )
    }
    post(model:any) {
        return this.http.post(`${environment.apiUrl}/providers`, model)
    }
    delete(id:number) {
        return this.http.delete(`${environment.apiUrl}/providers/${id}`)
    }

}