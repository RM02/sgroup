import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class CotizacionesService {
        
    constructor(private http: HttpClient) { }

    getAll(params?:any) {

        return this.http.get(`${environment.apiUrl}/quotations`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('perPage', '20')
            .set('sortField', 'id')
            .set('sortOrder', 'desc')
            .set('dataSearch', params.dataSearch ? JSON.stringify(params.dataSearch) : '')
        })
    }
    create(data:any) {
        return this.http.post(`${environment.apiUrl}/quotations`, data)
    }
    
    delete(id?:number) {
        return this.http.delete(`${environment.apiUrl}/quotations/${id}`)
    }
}