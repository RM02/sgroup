import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ExternalService {
    
    private readonly dniService = 'https://dni.optimizeperu.com/api/persons/'

    token = 'Bud0GfowMNDYeWD1HzNMZ6wZzWt1MXnyVNaGoplnu8hWiRNV4Wz5CC4f6Lr5'

    headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Authorization': `Bearer ${this.token}`
    })
    constructor(private http: HttpClient) { }

    getData(dni:number) {
        return this.http.get(`${this.dniService}${dni}`)
    }
    getRuc(ruc:any) {
    	return this.http.get(`${environment.apiUrl}/ruc/${ruc}`)
    }
    getExchangeValue(end_date?:any) {

        var ref = new Date(end_date)
        var start_date = new Date(ref.setDate(ref.getDate()-3))
        
        return this.http.get(`${environment.apiUrl}/exchange-rate`, {
            params: new HttpParams()
            .set('start_date', JSON.stringify(start_date))
            .set('final_date', JSON.stringify(end_date))
            .set('coin', 'coin')
        })
    }
}