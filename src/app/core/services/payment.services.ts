import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PaymentService {
        
    constructor(private http: HttpClient) { }

    getAll(params?:any) {

        return this.http.get(`${environment.apiUrl}/payment-methods`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('perPage', '20')
            .set('sortField', 'id')
            .set('sortOrder', 'desc')
            .set('dataSearch', params.dataSearch ? JSON.stringify(params.dataSearch): '')
        })
    }
    getDestinations() {
        return this.http.get(`${environment.apiUrl}/payment-destinations`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('perPage', '20')
            .set('sortField', 'id')
            .set('sortOrder', 'desc')
        })        
    }
}