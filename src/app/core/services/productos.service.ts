import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ProductosResponse, ProductosModel } from '../model/productos.interface';

import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ProductoService {
        
    constructor(private http: HttpClient) { }

    getProducts(params ?: any) {

        return this.http.get<any>(`${environment.apiUrl}/products`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('perPage', '100')
            .set('sortOrder', 'desc')
            .set('sortField', 'id')
            .set('dataSearch', params.dataSearch ? JSON.stringify(params.dataSearch): '')
        })
        .pipe(
            map((res:any) => {
                return res.map((products:any) => ProductosModel.productosFromJson(products))
            })
        )
    }
    filter(params?:any) {
        return this.http.get(`${environment.apiUrl}/products`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('perPage', '10')
            .set('sortOrder', 'desc')
            .set('sortField', 'id')
            .set('dataFilter', params.dataFilter ? JSON.stringify(params.dataFilter) : '')
            .set('filterProduct', params.filterProduct ? JSON.stringify(params.filterProduct): '')
        })
        .pipe(
            map((res:any) => {
                return res.map((prod:any) => ProductosModel.productosFromJson(prod))
            })
        )
    }

    addProduct(data:any) {
    	return this.http.post(`${environment.apiUrl}/products`, data)
    }

    getProductById(id:string) {
    	return this.http.get(`${environment.apiUrl}/products/${id}`)
    }

    updateProduct(data:any) {
        const id = data['id']
        return this.http.put(`${environment.apiUrl}/products/${id}`, data)
    }
    delete(id:any) {
        return this.http.delete(`${environment.apiUrl}/products/${id}`)
    }

}