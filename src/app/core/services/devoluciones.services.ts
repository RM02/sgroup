import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class DevolucionesService {
 
    constructor(private http: HttpClient) { }

    getAll(params?:any) {

        return this.http.get(`${environment.apiUrl}/devolutions`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('sortOrder', 'desc')
            .set('sortField', 'id')
            .set('perPage', '20')
            .set('dataFilter', params.dataFilter ? JSON.stringify(params.dataFilter): '')
        })
    }
    post(data:any) {
        return this.http.post(`${environment.apiUrl}/devolutions`, data)
    }
    delete(id:number) {
        return this.http.delete(`${environment.apiUrl}/devolutions/${id}`)
    }
}