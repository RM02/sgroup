import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ClientTypeService {
        
    constructor(private http: HttpClient) { }

    getClientTypes() {

        return this.http.get(`${environment.apiUrl}/client-types`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('perPage', '20')
            .set('sortField', 'id')
            .set('sortOrder', 'desc')
        })
    }
    getClientTypeById(id:number) {
    	return this.http.get(`${environment.apiUrl}/client-types/${id}`)
    }
}
