import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class VoucherTypeService {
        
    constructor(private http: HttpClient) { }

    getAllVouchers() {

        return this.http.get(`${environment.apiUrl}/voucher-types`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('sortOrder', 'desc')
            .set('sortField', 'id')
            .set('perPage', '20')
        })
    }
    post(data:any) {
        return this.http.post(`${environment.apiUrl}/bill-electronics`, data)
    }

}