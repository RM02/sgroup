import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class RolesService {
        
    constructor(private http: HttpClient) { }

    getAll() {

        return this.http.get(`${environment.apiUrl}/roles`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('perPage', '20')
            .set('sortField', 'id')
            .set('sortOrder', 'desc')
        })
    }
    create(model:any) {
        return this.http.post(`${environment.apiUrl}/roles`, model)
    }
    delete(id:number) {
        return this.http.delete(`${environment.apiUrl}/roles/${id}`)
    }
    update(data:any) {
        return this.http.put(`${environment.apiUrl}/roles`, data)
    }
}