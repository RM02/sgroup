import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PedidoService {
    
    private readonly service = environment.apiUrl;
    
    constructor(private http: HttpClient) { }

    getAll(params?: any) {
        return this.http.get(`${environment.apiUrl}/orders`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('perPage', '20')
            .set('sortOrder', 'desc')
            .set('sortField', 'id')
            .set('dataSearch', params ? JSON.stringify(params.dataSearch): '')
        })
    }
    post(data:any) {
        return this.http.post(`${environment.apiUrl}/orders`, data)
    }

    delete(id:number) {
        return this.http.delete(`${environment.apiUrl}/orders/${id}`)
    }
}