import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class OrdenesComprasService {
        
    constructor(private http: HttpClient) { }

    getAll(params?:any) {

        return this.http.get<any>(`${environment.apiUrl}/purchase-orders`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('sortOrder', 'desc')
            .set('sortField', 'id')
            .set('perPage', '20')
            .set('dataSearch', params.dataSearch ? JSON.stringify(params.dataSearch): '')
        })
    }
    post(data:any) {
        return this.http.post(`${environment.apiUrl}/purchase-orders`, data)
    }

    delete(id:number) {
        return this.http.delete(`${environment.apiUrl}/purchase-orders/${id}`)
    }
    getOne(id:number) {
        return this.http.get<any>(`${environment.apiUrl}/purchase-orders/${id}`)
    }
}