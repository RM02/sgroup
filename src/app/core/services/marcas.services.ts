import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class MarcasService {
        
    constructor(private http: HttpClient) { }

    getAll(params?: any) {
        return this.http.get(`${environment.apiUrl}/brands`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('perPage', '20')
            .set('sortOrder', 'desc')
            .set('sortField', 'id')
            .set('dataSearch', params.dataSearch ? JSON.stringify(params.dataSearch): ' ')
            .set('dataFilter', params.dataFilter ? JSON.stringify(params.dataFilter): ' ')
        })
    }
    updateBrand(data:any) {
    	const id = data['id']
        return this.http.put(`${environment.apiUrl}/brands/${id}`, data)
    }
    addBrand(data:any) {
        return this.http.post(`${environment.apiUrl}/brands`, data)
    }
    
    delete(id:number) {
        return this.http.delete(`${environment.apiUrl}/brands/${id}`)
    }
}