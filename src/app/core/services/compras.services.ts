import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CompraModel, Compra, ComprasResponse } from '../model/compras.interface';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ComprasService {
        
    constructor(private http: HttpClient) { }

    getAll(params?:any) {

        return this.http.get<ComprasResponse>(`${environment.apiUrl}/purchases`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('sortOrder', 'desc')
            .set('sortField', 'id')
            .set('perPage', '20')
            .set('dataSearch', params.dataSearch ? JSON.stringify(params.dataSearch): '')
        })
        .pipe(
            map(res => {
                return res.data.map(compra => CompraModel.comprasFromJson(compra))
            })
        )
    }
    add(data:any) {
        return this.http.post(`${environment.apiUrl}/purchases`, data)
    }

    delete(id:number) {
        return this.http.delete(`${environment.apiUrl}/purchases/${id}`)
    }
}