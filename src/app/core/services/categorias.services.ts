import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class CategoriasService {
        
    constructor(private http: HttpClient) { }

    getAll(params?:any) {

        return this.http.get(`${environment.apiUrl}/categories`,
            { params: new HttpParams()
            .set('paginate', params.paginate ? params.paginated.toString() : 'true')
            .set('perPage', params.perPage ? params.perPage.toString(): '20')
            .set('sortField', params.sortField ? params.sortField.toString(): 'id')
            .set('sortOrder', params.Order ? params.sortOrder.toString(): 'desc')
            .set('dataSearch', params.dataSearch ? JSON.stringify(params.dataSearch) : '')
            .set('dataFilter', params.dataFilter ? JSON.stringify(params.dataFilter) : '')
        })
    }
    updateCategory (data:any) {
    	const id = data.id
    	return this.http.put(`${environment.apiUrl}/categories/${id}`, data)
    }

    post(data:any) {
    	return this.http.post(`${environment.apiUrl}/categories`, data)
    }
    delete(id:number) {
    	return this.http.delete(`${environment.apiUrl}/categories/${id}`)
    }

}