import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ReporteKardexResponse, ReporteKardexModel } from '../model/reporte-kardex.interface';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ReporteKardexService {
        
    constructor(private http: HttpClient) { }

    getAll() {

        return this.http.get<ReporteKardexResponse>(`${environment.apiUrl}/kardex-reports`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('perPage', '20')
            .set('sortField', 'id')
            .set('sortOrder', 'desc')
        })
        .pipe(
            map((res:any) => {
                return res.data.map((reporte:any) => ReporteKardexModel.reporteFromJson(reporte))
            })
        )
    }
}