import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ClienteModel, ClienteResponse } from '../model/clientes.interface';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ClientService {

    constructor(private http: HttpClient) { }

    getAll(params?:any) {

        return this.http.get<ClienteResponse>(`${environment.apiUrl}/clients`,
            { params: new HttpParams()
                .set('paginate', 'true')
                .set('perPage', '20')
                .set('sortOrder', 'desc')
                .set('sortField', 'id')
                .set('dataSearch', params.dataSearch ? JSON.stringify(params.dataSearch): '')
                .set('dataFilter', params.dataFilter ? JSON.stringify(params.dataFilter): '')
            })
            .pipe(
                map(res => {
                    return res.data.map(user => ClienteModel.clienteFromJson(user))
                })
            )
    }
    postClient(data:any) {
    	return this.http.post(`${environment.apiUrl}/clients`, data)
    }
    getOne(id:number) {
        return this.http.get(`${environment.apiUrl}/clients/${id}`)
    }
    delete(id:number) {
        return this.http.delete(`${environment.apiUrl}/clients/${id}`)
    }
    updateClient(id:number, data:any) {
        return this.http.put(`${environment.apiUrl}/clients/${id}`, data)
    }
}