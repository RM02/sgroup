import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { TransferResponse, TransferModel } from '../model/transfer.interface';

@Injectable({
    providedIn: 'root'
})
export class TransfersService {
        
    constructor(private http: HttpClient) { }

    getAll(params?:any) {

        return this.http.get<TransferResponse>(`${environment.apiUrl}/transfers`,
            { params: new HttpParams()
            .set('paginate', params.paginate ? params.paginated.toString() : 'true')
            .set('perPage', params.perPage ? params.perPage.toString(): '20')
            .set('sortField', params.sortField ? params.sortField.toString(): 'id')
            .set('sortOrder', params.Order ? params.sortOrder.toString(): 'desc')
            .set('dataSearch', params.dataSearch ? JSON.stringify(params.dataSearch) : '')
            .set('dataFilter', params.dataFilter ? JSON.stringify(params.dataFilter) : '')
        })
        .pipe(
            map((res) => {
                return res.data.map(transfer => TransferModel.transferFromJson(transfer))
            })
        )
    }
    delete(id:number) {
    	return this.http.delete(`${environment.apiUrl}/transfers/${id}`)
    }
    post(data:any) {
    	return this.http.post(`${environment.apiUrl}/transfers`, data)
    }
    put(id:number, data:any) {
    	return this.http.put(`${environment.apiUrl}/transfers/${id}`, data)
    }
}