import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class CreditNoteTypesService {
        
    constructor(private http: HttpClient) { }

    getAll() {

        return this.http.get(`${environment.apiUrl}/type-of-credit-notes`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('perPage', '20')
            .set('sortField', 'id')
            .set('sortOrder', 'desc')
        })
    }
}