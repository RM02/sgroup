import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';
import { PdfMakeWrapper, Txt, Table, Columns, Img, Rect, Canvas } from 'pdfmake-wrapper';

//import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';

PdfMakeWrapper.setFonts(pdfFonts);

//(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;

// Set the fonts to use

@Injectable({
  providedIn: 'root'
})
export class GeneratorService {

  constructor() { }

  generatePDF (data:any) {
    
    const pdf = new PdfMakeWrapper();

    pdf.defaultStyle({
      fontSize: data.fontSize
    })
    
    pdf.pageOrientation(data.orientation ? data.orientation:  "landscape")
    pdf.pageSize(data.pageSize)

    pdf.header([
      new Txt(data.header).alignment("center").margin([0, 20]).end,
    ]);
      
    pdf.add(this.createTable(data.content, data.width ? data.width: "auto"))
    pdf.create().open()
  }
  
  exportExcel(data:any, title:string) {
    let filename = title + '.xlsx';
    const wb: XLSX.WorkBook = XLSX.utils.book_new();

    if (title == 'registro_ventas') {

      let ws0: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(data[0])
      let ws1: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(data[1])
      let ws2: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(data[2])

      XLSX.utils.book_append_sheet(wb, ws0, "boletas");
      XLSX.utils.book_append_sheet(wb, ws1, "facturas");
      XLSX.utils.book_append_sheet(wb, ws2, "créditos");

      
    } else {
      const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(data);
      XLSX.utils.book_append_sheet(wb, ws, 'sheet');
    }
    XLSX.writeFile(wb, filename);
  }
  async generateComprobante (data:any) {
    const pdf = new PdfMakeWrapper();

    pdf.pageSize(data.pageSize)
    pdf.defaultStyle({
      fontSize: data.fontSize,
      margin: [5, 5]
    })
  
    pdf.add([
      new Columns([
        'REPUESTOS\nGRUPO SUDAMÉRICA',
        '',
        `RUC/DNI: ${data.number}\nCOMPROBANTE ELECTRÓNICO`
      ])
      .alignment("justify")
      .bold()
      .fontSize(12)
      .end,
      new Columns([""])
      .margin([5, 5])
      .end,
      new Columns([`SEÑOR(ES): ${data.client}\nF. EMISIÓN: ${data.date}\nMONEDA: ${data.moneda}\n`])
      .alignment("left")
      .margin([15, 15])
      .fontSize(10)
      .end
    ])

    pdf.add(this.createTable(data.content, "*"))
    
    let igv = (data.total*0.18).toFixed(3)
    
    pdf.add([
      new Columns(['', 'TOTAL AGRAVADO:', ' 0'])
      .columnGap(1)
      .margin([0, 10, 0, 0])
      .alignment("right")
      .fontSize(10)
      .end
    ])
    pdf.add([
      new Columns(['', 'TOTAL NO GRAVADO:', '0'])
      .columnGap(1)
      .alignment("right")
      .margin([1, 1])
      .fontSize(10)
      .end
    ])
    pdf.add([
      new Columns(['', 'TOTAL IGV 18%:',  `${igv}`])
      .columnGap(1)
      .alignment("right")
      .margin([1, 1])
      .fontSize(10)
      .end
    ])
    pdf.add([
      new Columns(['', 'TOTAL A PAGAR:', `${data.total}`])
      .columnGap(1)
      .alignment("right")
      .margin([1, 1])
      .bold()
      .fontSize(10)
      .end
    ])
    pdf.create().open()
  }
  createTable(data:any, width?:any) {
    return new Table(data)
    .widths(width)
    .margin([10, 10])
    .layout("lightHorizontalLines")
    .end;
  }
}
