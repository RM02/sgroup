import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { WarehouseResponse, WarehouseModel } from '../model/warehouses.interface';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class WarehouseService {
        
    constructor(private http: HttpClient) { }

    getAll(params?:any) {

        return this.http.get<WarehouseResponse>(`${environment.apiUrl}/warehouses`,
            { params: new HttpParams()
            .set('paginate', params.paginate ? params.paginated.toString() : 'true')
            .set('perPage', params.perPage ? params.perPage.toString(): '20')
            .set('sortField', params.sortField ? params.sortField.toString(): 'id')
            .set('sortOrder', params.Order ? params.sortOrder.toString(): 'desc')
            .set('dataSearch', params.dataSearch ? JSON.stringify(params.dataSearch) : '')
            .set('dataFilter', params.dataFilter ? JSON.stringify(params.dataFilter) : '')
        })
        .pipe(
            map((res) => {
                return res.data.map(warehouses => WarehouseModel.warehouseFromJson(warehouses))
            })
        )
    }
    delete(id:number) {
    	return this.http.delete(`${environment.apiUrl}/warehouses/${id}`)
    }
    post(data:any) {
    	return this.http.post(`${environment.apiUrl}/warehouses`, data)
    }
    put(id:number, data:any) {
    	return this.http.put(`${environment.apiUrl}/warehouses/${id}`, data)
    }
}