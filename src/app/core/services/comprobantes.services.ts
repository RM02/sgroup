import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ComprobantesResponse, ComprobanteModel } from '../model/comprobantes.interface';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ComprobanteService {
        
    constructor(private http: HttpClient) { }

    getAll(params?:any) {

        return this.http.get<ComprobantesResponse>(`${environment.apiUrl}/bill-electronics`,
            { params: new HttpParams()
            .set('paginate', 'true')
            .set('sortOrder', 'desc')
            .set('sortField', 'id')
            .set('perPage', '20')
            .set('dataSearch', params.dataSearch ? JSON.stringify(params.dataSearch): '')
            .set('dataFilter', params.dataFilter ? JSON.stringify(params.dataFilter): '')
        })
        .pipe(
            map(res => {
                return res.data.map(items => ComprobanteModel.comprobanteFromJson(items))
            })
        )
    }
    note(id:number, data:any) {
        return this.http.post(`${environment.apiUrl}/bill-electronics/${id}/note`, data)
    }
    getOne(id:number) {
        return this.http.get(`${environment.apiUrl}/bill-electronics/${id}`)
    }

    post(data:any) {
        return this.http.post(`${environment.apiUrl}/bill-electronics`, data)
    }
    update(id:number, data:any) {
        return this.http.put(`${environment.apiUrl}/bill-electronics/${id}`, data)
    }
    delete(id:number) {
        return this.http.delete(`${environment.apiUrl}/bill-electronics/${id}`)
    }

}