import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../../../core/services/auth.services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  currentYear = new Date().getFullYear();

  validateForm!: FormGroup;

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
    if (this.validateForm.status == 'VALID') {
      this.auth.login(this.validateForm.value)
      .pipe(first())
      .subscribe(
        () => {
          let rol = this.auth.currentUserValue.user.roles[0].id
          switch (rol) {
            case 1:
            case 2:
            case 3:
            case 11:
              this.router.navigate(['ventas', 'listado-comprobante'])
              break;
            case 1:
            case 2:
            case 11:
              this.router.navigate(['inventario', 'almacen'])
              break;
            case 1:
            case 2:
            case 11:
              this.router.navigate(['compras', 'listado'])
              break;
            default:

              break;
          }
        }
      )
    }
  }
  constructor(
    private router: Router,
    private auth: AuthenticationService,
    private fb: FormBuilder
    ) {}
  ngOnInit(): void {
    this.auth.logout()
    this.validateForm = this.fb.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
    });
  }
}
