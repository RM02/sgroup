import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlmacenComponent } from './components/almacen/almacen.component';
import { InventarioComponent } from './inventario.component';
import { TrasladosComponent } from './components/traslados/traslados.component';
import { MovimientosComponent } from './components/movimientos/movimientos.component';
import { DevolucionesComponent } from './components/devoluciones/devoluciones.component';
import { ReporteKardexComponent } from './components/reporte-kardex/reporte-kardex.component';
import { ReporteInventarioComponent } from './components/reporte-inventario/reporte-inventario.component';
import { KardexValorizadoComponent } from './components/kardex-valorizado/kardex-valorizado.component';
import { CrearDevolucionComponent } from './components/crear-devolucion/crear-devolucion.component';

const routes: Routes = [
    {
        path: '',
        component: InventarioComponent,
        children:[
            {
                path: 'almacen', component: AlmacenComponent 
            },
            {
                path: 'traslados', component: TrasladosComponent 
            },
            {
                path: 'movimientos', component: MovimientosComponent 
            },
            {
                path: 'devoluciones', component: DevolucionesComponent 
            },
            {
                path: 'reporte-kardex', component: ReporteKardexComponent 
            },
            {
                path: 'reporte-inventario', component: ReporteInventarioComponent 
            },
            {
                path: 'kardex-valorizado', component: KardexValorizadoComponent 
            },
            {
                path: 'crear-devolucion', component: CrearDevolucionComponent
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class InventarioRoutingModule { }