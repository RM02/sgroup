import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../core/services/index';
import { MenuConfig } from './config/config';

@Component({
  selector: 'app-inventario',
  templateUrl: './inventario.component.html',
  styleUrls: ['./inventario.component.scss']
})
export class InventarioComponent implements OnInit {

  isCollapsed = false;
  currentYear: number = new Date().getFullYear();
  currentUser: any = {};
  menuConfig:any = MenuConfig;

  constructor(
    private auth: AuthenticationService
  ) {
    this.currentUser = this.auth.currentUserValue.user
   }

  ngOnInit(): void {
    this.hasAccesss()
  }
  hasAccesss() {
    this.menuConfig.map((section:any) => {
      if (section.roles && section.roles.indexOf(this.currentUser.roles[0].id) != -1) {
        section.access = true
      }
    })
  }

}
