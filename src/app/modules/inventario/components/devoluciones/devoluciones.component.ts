import { Component, OnInit } from '@angular/core';
import { DevolucionesService, GeneratorService, UsersService, AuthenticationService, ProductoService } from '../../../../core/services/index';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-devoluciones',
  templateUrl: './devoluciones.component.html',
  styleUrls: ['./devoluciones.component.scss']
})
export class DevolucionesComponent implements OnInit {
  
  isLoading:boolean = false;
  showFilter:boolean = false;
  isVisibleDetails: boolean = false;
  export: boolean = false;
  isVisibleModal: boolean = false;
  dataTable: any = [];
  productList: any = [];
  usersList: any = [];
  productDetails: any = {};
  selectedProduct: any = {};
  params: any = {
    dataFilter: ''
  };
  details: any = [];

  constructor(
      private apiProduct: ProductoService,
      private auth: AuthenticationService,
      private apiUsers: UsersService,
      private generator: GeneratorService,
      private notification: NzNotificationService,
      private apiDevoluciones: DevolucionesService
    ) { }

  ngOnInit(): void {
    this.getDevoluciones()
    this.getUsers()
    this.getProducts()
  }
  getDevoluciones() {
    this.isLoading = true
    this.apiDevoluciones.getAll(this.params).subscribe((res:any)=> {
      this.createNotification('success', 'Notificación', 'Se han cargado los registros exitosamente!')
      this.dataTable = res['data']
      this.isLoading = false
    })
  }
  getUsers() {
    this.apiUsers.getAll().subscribe((res:any) => {
      this.usersList = res
    })
  }
  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
  getProducts() {
    this.apiProduct.getProducts(this.params).subscribe((data:any) => {
      this.productList = data
    })
  }
  filterBy(key:string, value:any) {
    value = value.target.value.toString().toUpperCase()
    let params = {
      dataFilter: {
        [key]: value
      }
    }
    this.apiDevoluciones.getAll(params).subscribe((res:any) => {
      this.dataTable = res['data']
    })
  }
  showDetails(data:any) {
    this.details = data.devolution_details
    this.isVisibleDetails = true
  }
  handleCancelStock() {
    this.isVisibleDetails = false
  }
  handleOkStock() {
    this.isVisibleDetails = false
  }
  onSearchProduct(event:any) {

  }
  handleCancelMiddle() {
    this.isVisibleDetails = false
  }
  handleOkMiddle() {
    this.isVisibleDetails = false
  }
  handleAdd() {
    this.isVisibleModal = true
  }
  delete(id:number) {
    this.apiDevoluciones.delete(id).subscribe((res:any) => {
      this.getDevoluciones()
    })
  }
  downloadPdf() {
    let content = this.pdfExcelModel()
    let pdf = {
      header: "REPUESTOS | GRUPO SUDAMÉRICA",
      pageSize: 'A4',
      fontSize: 10,
      width: "*",
      orientation: "portrait",
      content: content
    }
    this.generator.generatePDF(pdf)
  }
  downloadExcel() {
    let content = this.pdfExcelModel()
    this.generator.exportExcel(content, "Registro devoluciones")
    this.export = false
  }
  pdfExcelModel() {
    let model = [['Item', 'Usuario', 'Devolución', 'Motivo']]
    let n = 0
    this.dataTable.map((item:any) => {
      n += 1
      let user = `${item.user.name} ${item.user.last_name}`
      model.push([n, user, item.code, item.devolution_reason.name])
    })
    return model
  }
  visibleFilter() {
    this.showFilter = !this.showFilter
  }
}
