import { Component, OnInit } from '@angular/core';
import { TransfersService } from '../../../../core/services/index';

@Component({
  selector: 'app-traslados',
  templateUrl: './traslados.component.html',
  styleUrls: ['./traslados.component.scss']
})
export class TrasladosComponent implements OnInit {

	isLoading: boolean = false;
  isVisibleDetail: boolean = false;
  detailsList: any = {};
	params: any = {};
	dataTable: any = [];
	
  constructor(
  	private apiTransfer: TransfersService
  	) { }

  ngOnInit(): void {
  	this.getTransfers()
  }
  delete(id:number) {
    this.apiTransfer.delete(id).subscribe((res:any) => {
      this.getTransfers()
    })
  }
  getTransfers() {
  	this.isLoading = true
  	this.apiTransfer.getAll(this.params).subscribe((data:any) => {
  		this.dataTable = data
  		this.isLoading = false
  	})
  }
  showDetails(data:any) {
    this.isVisibleDetail = true
    this.detailsList = data.transfer_details
  }
  handleOk() {
    this.isVisibleDetail = false
  }
  handleCancel() {
    this.isVisibleDetail = false
  }
}
