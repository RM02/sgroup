import { Component, OnInit } from '@angular/core';
import { AuthenticationService, ReporteKardexService } from '../../../../core/services/index';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-reporte-kardex',
  templateUrl: './reporte-kardex.component.html',
  styleUrls: ['./reporte-kardex.component.scss']
})
export class ReporteKardexComponent implements OnInit {
	isLoading: boolean = false;
	dataTable: any = [];
	isVisibleModal: boolean = false;
	modalTitle: string = '';
	showFilter: boolean = false;
	model: any = {};

  constructor(
  	private auth: AuthenticationService,
  	private apiReport: ReporteKardexService,
    private notification: NzNotificationService
  	) { }

  ngOnInit(): void {
  	this.getReports()
  }
  getReports() {
  	this.isLoading = true
  	this.apiReport.getAll().subscribe((data) => {
      this.createNotification('success', 'Notificación', 'Se han cargado los registros exitosamente!')
  		this.dataTable = data
  		this.isLoading = false
  	})
  }
  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
  handleAdd() {

  }
  handleCancel() {

  }
  handleFilter() {
    this.showFilter = !this.showFilter
  }
  handleOk() {

  }
  filterBy(key:string, event:any) {

  }

}
