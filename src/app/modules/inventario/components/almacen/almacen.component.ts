import { Component, OnInit } from '@angular/core';
import { WarehouseService, AuthenticationService } from '../../../../core/services/index'
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-almacen',
  templateUrl: './almacen.component.html',
  styleUrls: ['./almacen.component.scss']
})
export class AlmacenComponent implements OnInit {

  dataTable: any = [];
  isLoading: boolean = false;
  isVisibleModal: boolean = false;
  showFilter: boolean = false;

  currentUser: any = {};

  modalTitle:string = 'Agregar nuevo establecimiento';
  model: any = {};
  params: any = {};

  constructor(
    private notification: NzNotificationService,
    private apiOffice: WarehouseService,
    private auth: AuthenticationService
  ) {
    this.currentUser = this.auth.currentUserValue.user
  }

  ngOnInit(): void {
    this.getHouses()
  }
  getHouses() {
    this.isLoading = true
    this.apiOffice.getAll(this.params).subscribe((data:any) => {
      this.createNotification('success', 'Notificación', 'Se han cargado los registros exitosamente!')
      this.dataTable = data
      this.isLoading = false
    })
  }
  
  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
  delete(id:number) {
    this.apiOffice.delete(id).subscribe((res:any) => {
      this.getHouses()
    })
  }
  filterBy(key:string, value:any) {

  }
  handleFilter() {
    this.showFilter = !this.showFilter
  }
  handleCancel() {
    this.isVisibleModal = false
  }
  handleOk() {
    let model = {
      ...this.model,
      branch_office_id: this.currentUser.branch_offices[0].pivot.branch_office_id,
      user_created_id: this.currentUser.id,
      user_updated_id: this.currentUser.id
    }
    if(this.modalTitle == 'Agregar nuevo establecimiento') {
      this.add(model)
    } else {
      this.update(model)
    }
    this.isVisibleModal = false
  }
  add(model:any) {
    this.apiOffice.post(model).subscribe((res:any) => {
      this.getHouses()
    })
  }
  edit(data:any) {
    this.modalTitle = 'Editar establecimiento'
    this.model = {
      ...data
    }
    this.isVisibleModal = true
  }
  update(data:any) {
    this.apiOffice.put(data.id, data).subscribe((res:any) => {
      this.getHouses()
    })
  }
  handleAdd() {
    this.model = {}
    this.isVisibleModal = true
  }

}
