import { Component, OnInit } from '@angular/core';
import { ProductoService, AuthenticationService } from '../../../../core/services/index';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-reporte-inventario',
  templateUrl: './reporte-inventario.component.html',
  styleUrls: ['./reporte-inventario.component.scss']
})
export class ReporteInventarioComponent implements OnInit {

	showFilter: boolean = false;
	isLoading: boolean = false;
	params: any = {};
	filter: any = {};
	dataTable: any = [];

  constructor(
  	private apiProduct: ProductoService
  	) { }

  ngOnInit(): void {
  	this.getProducts()
  }
  getProducts() {
  	this.apiProduct.getProducts(this.params).subscribe((res:any) => {
  		this.dataTable = res
  	})
  }
  filterBy(key:string, value: any) {
    this.isLoading = true
    value = value.target.value.toString().toUpperCase()

    switch (key) {
      case 'code':
        this.filter['description'] = value
        break;
      case 'category.name':
        this.filter['category.name'] = value
        break;
      default:
        break
    }
    let params = {
      dataFilter: Object.keys(this.filter).length > 0 ? this.filter: '',
    }
    this.apiProduct.filter(params).subscribe((res:any) => {
      this.dataTable = res
      this.isLoading = false
    })
  }
  handleFilter() {
  	this.showFilter = !this.showFilter
  }

}
