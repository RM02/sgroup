import { Component, OnInit } from '@angular/core';
import { 
  AuthenticationService,
  DevolucionReasonService,
  DevolucionesService,
  ProductoService } from '../../../../core/services/index';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crear-devolucion',
  templateUrl: './crear-devolucion.component.html',
  styleUrls: ['./crear-devolucion.component.scss']
})
export class CrearDevolucionComponent implements OnInit {
  
  currentUser: any = {};
  isVisibleModal: boolean = false;
  isConfirmed: boolean = false;
  observation: string = '';
  selectedReason: any = {};
  params: any = {};
  dataTable: any = [];
  productDetail: any = {};
  reasonList: any = [];
  productList: any = [];
  selectedProduct: any = {};

  date: Date = new Date();

  constructor(
    private auth: AuthenticationService,
    private apiProduct: ProductoService,
    private apiDevo: DevolucionesService,
    private apiReason: DevolucionReasonService,
    private router: Router
    ) { 
    this.currentUser = this.auth.currentUserValue.user
  }

  ngOnInit(): void {
    this.getReasons()
    this.getProducts()
  }
  getProducts() {
    this.apiProduct.getProducts(this.params).subscribe((data:any) => {
      this.productList = data
    })
  }
  getReasons() {
    this.apiReason.getAll().subscribe((res:any) => {
      this.reasonList = res['data']
    })
  }
  onSearchReason(value:string) { }
  onSearchProduct(value:string) { }

  deleteProduct(index:number) {
    this.dataTable.splice(index, 1)
    this.dataTable = [
     ...this.dataTable
    ]
  }
  handleOk() {
    let product = {
      product_id: this.selectedProduct.id,
      product_name: this.selectedProduct.name,
      amount: parseFloat(this.productDetail.amount.target.value),
      user_created_id: this.currentUser.id
    }
    this.dataTable = [
      ...this.dataTable,
      product
    ]
    this.isVisibleModal = false
  }
  save() {
    let model = {
      observation: this.observation,
      branch_office_id: this.currentUser.branch_offices[0].id,
      devolution_reason_id: this.selectedReason.id,
      devolution_details: this.dataTable,
      user_created_id: this.currentUser.id,
      created_at: this.date      
    }
    this.apiDevo.post(model).subscribe((res:any) => {
      this.isConfirmed = true
    })
  }
  navigateTo() {
    this.router.navigate(['inventario/devoluciones'])
  }
  handleCancelConfirm() {
    this.observation = ''
    this.selectedReason = {}
    this.isConfirmed = false
  }
  handleCancel() {
    this.isVisibleModal = false
  }
}
