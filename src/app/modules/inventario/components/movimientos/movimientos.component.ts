import { Component, OnInit } from '@angular/core';
import { ProductoService, WarehouseService, TransfersService, AuthenticationService } from '../../../../core/services/index';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-movimientos',
  templateUrl: './movimientos.component.html',
  styleUrls: ['./movimientos.component.scss']
})
export class MovimientosComponent implements OnInit {

	isLoading:boolean = false;
  showTransferModal: boolean = false;
  selectedItem: any = {};
  isVisibleStock: boolean = false;
	dataTable: any = [];
  warehouseList: any = [];
  params: any = {};
	showFilter: boolean = false;
  currentUser: any = {};

  constructor(
    private apiProduct: ProductoService,
    private apiWarehouse: WarehouseService,
    private auth: AuthenticationService,
    private apiTransfer: TransfersService,
    private notification: NzNotificationService
    ) {
      this.currentUser = this.auth.currentUserValue.user
    }

  ngOnInit(): void {
    this.getProducts()
    this.getWarehouses()
  }
  getProducts() {
    this.isLoading = true
    this.apiProduct.getProducts(this.params).subscribe((res:any) => {
      this.createNotification('success', 'Notificación', 'Se han cargado los registros exitosamente!')
      this.dataTable = res
      this.isLoading = false
    })
  }
  getWarehouses() {
    this.apiWarehouse.getAll(this.params).subscribe((res:any) => {
      this.warehouseList = res
    })
  }
  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
  handleAdd() {

  }
  handleFilter() {

  }
  filterBy(key:string, event:any) {

  }
  transfer(data:any) {
    this.showTransferModal = true
    this.selectedItem = data
  }
  delete(id:number) {

  }
  handleOkStock() {
    let model = {
      from_warehouse_id: this.selectedItem['from_warehouse_id'],
      to_warehouse_id: this.selectedItem['to_warehouse_id'],
      transferDetails: [
        {
          "product_id": this.selectedItem['id'],
          "amount": parseInt(this.selectedItem['amount']),
          "user_created_id": this.currentUser.id
        }
      ],
      user_created_id: this.currentUser.id,
      user_updated_id: this.currentUser.id
    }
    this.apiTransfer.post(model).subscribe((res:any) => {
      this.showTransferModal = false
      this.getProducts()
    })
  }
  handleCancelStock() {
    this.showTransferModal = false
  }
}
