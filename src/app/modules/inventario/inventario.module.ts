import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventarioRoutingModule } from './inventario-routing.module';

import { AlmacenComponent } from './components/almacen/almacen.component';
import { InventarioComponent } from './inventario.component';
import { TrasladosComponent } from './components/traslados/traslados.component';
import { MovimientosComponent } from './components/movimientos/movimientos.component';
import { DevolucionesComponent } from './components/devoluciones/devoluciones.component';
import { ReporteKardexComponent } from './components/reporte-kardex/reporte-kardex.component';
import { ReporteInventarioComponent } from './components/reporte-inventario/reporte-inventario.component';
import { KardexValorizadoComponent } from './components/kardex-valorizado/kardex-valorizado.component';
import { CrearDevolucionComponent } from './components/crear-devolucion/crear-devolucion.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IconsProviderModule } from '../../icons-provider.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';

import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzResultModule } from 'ng-zorro-antd/result';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';

@NgModule({
  declarations: [
    AlmacenComponent,
    InventarioComponent,
    TrasladosComponent,
    MovimientosComponent,
    DevolucionesComponent,
    ReporteKardexComponent,
    ReporteInventarioComponent,
    KardexValorizadoComponent,
    CrearDevolucionComponent
  ],
  imports: [
    CommonModule,
    InventarioRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    IconsProviderModule,
    NzLayoutModule,
    NzMenuModule,
    NzDatePickerModule,
    NzTableModule,
    NzDividerModule,
    NzButtonModule,
    NzSpaceModule,
    NzGridModule,
    NzInputModule,
    NzCardModule,
    NzSelectModule,
    NzModalModule,
    NzResultModule,
    NzNotificationModule
  ]
})
export class InventarioModule { }
