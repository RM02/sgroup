import { Component, OnInit } from '@angular/core';
import { RolesService, AuthenticationService } from '../../../../core/services/index';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {

  dataTable: any = [];
  model: any = {};
  isLoading: boolean = false;
  isVisibleModal: boolean = false;
  modalTitle: string = 'Agregar nuevo rol';
  currentUser: any;

  constructor(
    private apiRoles: RolesService,
    private auth: AuthenticationService
  ) { 
    this.currentUser = this.auth.currentUserValue.user
  }

  ngOnInit(): void {
    this.getRoles()
  }
  getRoles() {
    this.isLoading = true
    this.apiRoles.getAll().subscribe((res:any) => {
      this.dataTable = res['data']
      this.isLoading = false
    })
  }
  handleCancel() {
    this.isVisibleModal = false
  }
  handleOk() {
    this.isVisibleModal = false

    if (this.modalTitle == 'Agregar nuevo rol') {
      this.add()
    }
    else if (this.modalTitle == 'Editar rol') {
      this.update()
    }
  }
  add() {
    let model = {
      ...this.model,
      user_created_id: this.currentUser.id
    }
    this.apiRoles.create(model)
    .subscribe((res) => {
      this.getRoles()
    })
  }
  update() {
    let model = {
      ...this.model,
      user_updated_id: this.currentUser.id
    }
    this.apiRoles.update(model).subscribe((res) => {
      this.getRoles()
    })
  }
  showAdd() {
    this.model = {}
    this.modalTitle = 'Agregar nuevo rol'
    this.isVisibleModal = true
  }
  edit(data:any) {
    this.modalTitle = 'Editar rol'
    this.model = data
    this.isVisibleModal = true
  }
  delete(id:number) {
    this.apiRoles.delete(id).subscribe((res) => {
      this.getRoles()
    })
  }
}
