import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../../core/services/index'
@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {

  dataTable: any = [];
  isLoading: boolean =  false;

  constructor(
    private apiUsers: UsersService
  ) { }

  ngOnInit(): void {
    this.getUsers()
  }
  getUsers() {
    this.apiUsers.getAll().subscribe((res:any) => {
      this.dataTable = res['data']
    })
  }
  delete(id:number) {

  }
}
