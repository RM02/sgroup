import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { ConfiguracionComponent } from './configuracion.component';
import { RolesComponent } from './components/roles/roles.component';

const routes: Routes = [
    { 
        path: '',
        component: ConfiguracionComponent,
        children: [
            {
                path: 'usuarios', component: UsuariosComponent
            },
            {
                path: 'roles', component: RolesComponent
            }
        ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfiguracionRoutingModule { }
