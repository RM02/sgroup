import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListadoComponent } from './components/listado/listado.component';
import { NuevaCompraComponent } from './components/nueva-compra/nueva-compra.component';
import { ComprasComponent } from './compras.component';
import { ProveedoresComponent } from './components/proveedores/proveedores.component';
import { SharedComponent } from './components/shared/shared.component';
import { OrdersComponent } from './components/orders/orders.component';
import { CreateOrderComponent } from './components/create-order/create-order.component';

const routes: Routes = [
    {
        path: '',
        component: ComprasComponent,
        children: [
            { path: 'listado', component: ListadoComponent },
            { path: 'nueva-compra', component: NuevaCompraComponent },
            { path: 'proveedores', component: ProveedoresComponent },
            { path: 'ordenes', component: OrdersComponent },
            { path: 'crear-orden', component: CreateOrderComponent },
            { path: 'agregar-productos/:id', component: SharedComponent }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class ComprasRoutingModule { }