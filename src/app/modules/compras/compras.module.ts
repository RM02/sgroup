import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ComprasRoutingModule } from './compras-routing.module';

import { NuevaCompraComponent } from './components/nueva-compra/nueva-compra.component';
import { ListadoComponent } from './components/listado/listado.component';
import { ComprasComponent } from './compras.component';

import { IconsProviderModule } from '../../icons-provider.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzResultModule } from 'ng-zorro-antd/result';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';

import { ProveedoresComponent } from './components/proveedores/proveedores.component';
import { SharedComponent } from './components/shared/shared.component';
import { OrdersComponent } from './components/orders/orders.component';
import { CreateOrderComponent } from './components/create-order/create-order.component';

@NgModule({
  declarations: [
    NuevaCompraComponent,
    ListadoComponent,
    ComprasComponent,
    ProveedoresComponent,
    SharedComponent,
    OrdersComponent,
    CreateOrderComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ComprasRoutingModule,
    IconsProviderModule,
    NzLayoutModule,
    NzTableModule,
    NzMenuModule,
    NzDividerModule,
    NzButtonModule,
    NzSpaceModule,
    NzGridModule,
    NzInputModule,
    NzCardModule,
    NzSelectModule,
    NzDatePickerModule,
    NzModalModule,
    NzResultModule,
    NzNotificationModule,
    NzCheckboxModule,
    NzAlertModule,
    NzInputNumberModule
  ]
})
export class ComprasModule { }
