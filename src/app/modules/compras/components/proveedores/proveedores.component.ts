import { Component, OnInit } from '@angular/core';
import { ProvidersService, DocumentTypeService, ClientTypeService, ExternalService, AuthenticationService } from '../../../../core/services/index';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.component.html',
  styleUrls: ['./proveedores.component.scss']
})
export class ProveedoresComponent implements OnInit {
  
  isLoading: boolean = false;
  showStatus: boolean = false;
  isVisibleModal: boolean = false;
  searchLabel: string = 'SUNAT';
  noRegister: boolean = false;

  selectedDocument: any = {};
  clienteTypeList: any = [];
  documentTypeList: any = [];
  selectedClienteType: any = {};
  model: any = {};
  currentUser: any = {};

  dataTable: any = [];
  constructor(
    private apiProvider: ProvidersService,
    private apiExternal: ExternalService, 
    private apiClienteType: ClientTypeService,   
    private apiDocumentType: DocumentTypeService,
    private notification: NzNotificationService,
    private apiAuth: AuthenticationService
  ) {
    this.currentUser = this.apiAuth.currentUserValue['user']
   }

  ngOnInit(): void {
    this.getProviders()
    this.getClienteType()
    this.getDocumentType()
  }
  getProviders() {
    this.isLoading = true
    this.apiProvider.getProviders().subscribe((res:any) => {
      this.isLoading = false
      this.dataTable = res
      this.createNotification('success', 'Notificación', 'Se han cargado los registros exitosamente!')
    })
  }
  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
  delete(id:number) {
    this.apiProvider.delete(id).subscribe((res:any) => {
      this.getProviders()
    })
  }
  handleCancel() {
    this.isVisibleModal = false
  }
  onSelectedDocument(value:string) {

  }
  handleOk() {
    let provider = {
      name: this.model['name'],
      last_name: "string",
      full_name_contact: this.model['full_name_contact'],
      residence_condition: this.model['residence_condition'],
      phone: this.model['phone'],
      phone_contact: this.model['phone_contact'],
      document_type_id: this.selectedDocument.id,
      document_number: this.model['document_number'],
      email: this.model['email'],
      user_created_id: this.currentUser.id,
      user_updated_id: this.currentUser.id
    }
    this.apiProvider.post(provider).subscribe(
      res => {
        this.isVisibleModal = false
        this.getProviders()
    },
    err => {
      this.createNotification('error', 'Error', 'Error del servidor')
    })
    this.isVisibleModal = false
  }
  handleAdd() {
    this.isVisibleModal = true
  }
  searchDniRuc() {
    if (this.selectedDocument.name == 'RUC') {
      this.apiExternal.getRuc(this.model['document_number']).subscribe((res:any) => {
        if (res.success) {
          this.showStatus = res.estado_del_contribuyente == 'ACTIVO' ? true : false;
          this.noRegister = false
          this.model['residence_condition'] = res.condicion_de_domicilio,
          this.model['address'] = res['direccion']
          this.model['name'] = res['nombre_o_razon_social']
        } else {
          this.noRegister = true
          this.showStatus = false
        }
      })
    }
    else if (this.selectedDocument.name == 'DNI') {
      // this.showLabelLastName = true
      this.apiExternal.getData(this.model['document_number']).subscribe((client:any) => {
        if (client['name']) {
          this.model['name'] = client['name']
          this.model['last_name'] = `${client['first_name']} ${client['last_name']}`
        } else {
          this.noRegister = true
        }
      })
    }
  }
  getClienteType() {
    this.apiClienteType.getClientTypes().subscribe((res:any) => {
      this.selectedClienteType = res['data'][1]
      this.clienteTypeList = res['data']
    })
  }
  getDocumentType() {
    this.apiDocumentType.getAll().subscribe((res:any) => {
      this.selectedDocument = res['data'][0]
      this.documentTypeList = res['data']
    })
  }
  showAddModal() {
    this.isVisibleModal = true
  }
}
