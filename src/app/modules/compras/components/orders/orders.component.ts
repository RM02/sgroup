import { Component, OnInit } from '@angular/core';
import { OrdenesComprasService, AuthenticationService } from '../../../../core/services/index';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

	params: any = {};
	dataTable:any = [];
	showFilter: boolean = false;
	isLoading: boolean = false;

  constructor(
  	private apiOrdenes: OrdenesComprasService,
  	private auth: AuthenticationService,
    private router: Router
  ) { }

  ngOnInit(): void {
  	this.getOrdenes()
  }
  getOrdenes() {
  	this.isLoading = true
  	this.apiOrdenes.getAll(this.params).subscribe((res:any) => {
  		this.isLoading = false
  		this.dataTable = res['data']
      this.dataTable.map((item:any) => {
        let total = 0
        item.purchase_order_details.map((element:any) => {
          total += element.purchase_price
        })
        item.total = total
      })
  	})
  }
  filterBy(value:any, event:any) {}
  delete(id:number) {
    this.apiOrdenes.delete(id).subscribe((res:any) => {
      this.getOrdenes()
    })
  }
  generarCompra(data:any) {
    this.router.navigate(['/compras/nueva-compra', {id: data.id}])
  }
}
