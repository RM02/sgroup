import { Component, OnInit } from '@angular/core';
import { AuthenticationService, ComprasService } from '../../../../core/services/index';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { CompraModel, Compra } from '../../../../core/model/compras.interface';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss']
})
export class ListadoComponent implements OnInit {

  export: boolean = false;
  productsDetails: any = [];
  isVisibleProduct: boolean = false;
  params: any = {};
  showFilter: boolean = false;
  dataTable: any = [];
  
  isLoading: boolean = false;
  constructor(
    private notification: NzNotificationService,
    private apiCompras: ComprasService,
    private auth: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.getCompras()
  }
  getCompras() {
    this.isLoading = true
    this.apiCompras.getAll(this.params).subscribe(res => {
      this.dataTable = res
      this.isLoading = false
      this.createNotification('success', 'Notificación', 'Se han cargado los registros exitosamente!')
    })
  }
  showProduct(data:any) {
    this.productsDetails = data.purchase_details
    this.isVisibleProduct = true
  }
  handleCancel () {
    this.isVisibleProduct = false
  }
  handleOk() {
    this.isVisibleProduct = false
  }
  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
  delete(id:number) {
    this.apiCompras.delete(id).subscribe((res:any) => {
      this.getCompras()
    })
  }
  visibleFilter() {
    this.showFilter = !this.showFilter
  }
  filterBy(key:string, value:any) {
    value = value.target.value.toString()
    let params = {
      dataSearch: {
        [key]: value
      }
    }
    this.apiCompras.getAll(params).subscribe((res:any) => {
      this.dataTable = res
    })
  }
}
