import { Component, OnInit } from '@angular/core';
import { AuthenticationService, OrdenesComprasService, ProvidersService, ExternalService, PaymentService, CoinService } from '../../../../core/services/index';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.scss']
})
export class CreateOrderComponent implements OnInit {

	isConfirmed: boolean = false;
	selectedProvider: any = {};
	selectedPaymentMethod: any = {};

	exchange_rate: number = 0;
    currentCoin: string = 'S/';
    total: number = 0;
	effective_date: Date = new Date();
	expiration_date: Date = new Date();
	
	params: any = {};
	model: any = {};
	dataTable:any = [];
	providerList: any = [];
	coinList: any = [];
	paymentMethodList: any = [];

	currentUser: any = {};

  constructor(
  	private auth: AuthenticationService,
  	private apiExternal: ExternalService,
  	private apiCoin: CoinService,
  	private router: Router,
  	private apiOrden: OrdenesComprasService,
  	private apiPayments: PaymentService,
  	private apiProvider: ProvidersService

  ) {
  		this.currentUser = this.auth.currentUserValue.user
  		this.model['expiration_date'] = this.expiration_date
  		this.model['created_at'] = this.effective_date
    	this.currentCoin = this.model['coin'] ? this.model['coin'].symbol : 'S/'
  	}

  ngOnInit(): void {
  	this.getProviders()
  	this.getCoin()
  	this.getPaymentMethod()
    this.getExchange(this.effective_date)
    this.getStore()
  }
  getProviders() {
    this.apiProvider.getProviders(this.params).subscribe((data:any) => {
      this.providerList = data
    })
  }
  getCoin() {
    this.apiCoin.getAll().subscribe((res:any) => {
      this.model['coin'] = res['data'][0]
      this.coinList = res['data']
    })
  }
  getExchange(date:Date) {
    this.model['created_at'] = date
    this.getExchangeValues(date)
  }
  getExchangeValues (date:any) {
    this.apiExternal.getExchangeValue(date).subscribe((res: any) => {
      
      if (res['exchange_rates'].length > 0 && res['exchange_rates']) {
        let i = res['exchange_rates'].length - 1
        this.exchange_rate = res['exchange_rates'][i]['venta']
      }
    })
  }
  getPaymentMethod() {
    this.apiPayments.getAll(this.params).subscribe((res:any) => {
      this.selectedPaymentMethod = res['data'][0]
      this.paymentMethodList = res['data']
    })
  }
  onSearchProvider(event:any) {}
  onSearchPayment(event:any) {}
  delete(index:number) {
  	this.dataTable.splice(index, 1)
    this.dataTable = [
     ...this.dataTable
    ]
  }
  navigateTo(route:string) {
  	this.router.navigate([route])
  }
  getStore() {
    let selectedProducts = JSON.parse(localStorage.getItem('orden-productos') || '{}') 
    if (selectedProducts.data) {
      this.dataTable = selectedProducts['data'].map((item:any) => {
        let product = {
          product_id: item.id,
          warehouse_id: this.currentUser.branch_offices[0].pivot.branch_office_id,
          description: item.description,
          price: item.price,
          purchase_price: item.price,
          igv: 12,
          total: item.amount * item.price,
          amount: item.amount
        }
        this.total += product.total
        return product
      })
    }
  }
  save() {
  	let model = {
  		coin_id: this.model['coin'].id,
  		exchange_rate: this.exchange_rate.toString(),
  		provider_id: this.selectedProvider.id,
  		igv: "12",
  		payment_method_id: this.selectedPaymentMethod.id,
  		expiration_date: `${this.expiration_date.getFullYear()}-${this.expiration_date.getMonth()}-${this.expiration_date.getDay()}`,
  		purchase_order_details: this.dataTable.map((item:any) => {
  			return {
  				product_id: item.product_id,
  				warehouse_id: item.warehouse_id,
  				amount: item.amount,
  				purchase_price: item.purchase_price,
  				price: item.price,
  				igv: item.igv,
  				user_created_id: this.currentUser.id
  			}
  		}),
  		user_created_id: this.currentUser.id,
  		user_updated_id: this.currentUser.id,
  		created_at: this.effective_date
  	}
  	this.apiOrden.post(model).subscribe((res:any) => {
  		this.isConfirmed = true
      localStorage.setItem('orden-productos', '{}')
  	})
  }
  handleCancelConfirm() {
  	this.isConfirmed = false
  	this.dataTable = []
  }
}
