import { Component, OnInit } from '@angular/core';
import { 
  AtributoTypeService,
  ProductoService,
  CategoriasService,
  MarcasService,
  AuthenticationService,
  GeneratorService
} from '../../../../core/services';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';

import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime, map, switchMap, filter } from 'rxjs/operators';
import { Stock } from '../../../../core/model/productos.interface';
import { NzNotificationService } from 'ng-zorro-antd/notification';

interface FilterModel {
  "attributeTypeProducts.attribute_type_id": string | null,
  "attributeTypeProducts.description": string | null
}

@Component({
  selector: 'app-shared',
  templateUrl: './shared.component.html',
  styleUrls: ['./shared.component.scss']
})
export class SharedComponent implements OnInit {
  
  searchProductos$ = new BehaviorSubject('');
  searchAttr$ = new BehaviorSubject('');

  previousUrl: number = 0;
  indeterminate = false;
  showModal: boolean = false;
  stock: any[] = [];
  productLimit: number = 0;
  isVisibleStock: boolean = false;
  showFilter: boolean = true;
  filterLabel:string = "[+ Filtro]"
  dataTable: any = [];
  selectedProductsList:any = [];
  selectedFromList: any = {};
  selectedAmount: number = 1;
  params = {};
  model: any = {
    code: '',
    name: '',
  };
  dataFilter: FilterModel[] = [
    {
      "attributeTypeProducts.attribute_type_id": "1",
      "attributeTypeProducts.description": null
    },
    {
      "attributeTypeProducts.attribute_type_id": "9",
      "attributeTypeProducts.description": null
    },
    {
      "attributeTypeProducts.attribute_type_id": "6",
      "attributeTypeProducts.description": null
    }
  ];
  otherFilter:any = {};
  isLoading: boolean  = false;
  currentUser: any;

  constructor(
    private apiAttr: AtributoTypeService,
    private apiMarcas: MarcasService,
    private apiCategorias: CategoriasService,
    private notification: NzNotificationService,
    private apiProductos: ProductoService,
    private auth: AuthenticationService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private location: Location,
    private generator: GeneratorService
  ) {
    this.currentUser = this.auth.currentUserValue.user
   }

  ngOnInit(): void {
    this.getProductos()
    this.previousUrl = parseInt(this.activeRoute.snapshot.paramMap.get('id') || '0', 10)
  }
  getStore() {
    let store = null
    switch (this.previousUrl) {
      case 2:
        // code...
        store = JSON.parse(localStorage.getItem('products-compra') || '{}')
        break;
      case 1:
        store = JSON.parse(localStorage.getItem('orden-productos') || '{}')
        break
      default:
        break;
    }
    if (store && store.data) {
      this.selectedProductsList = store.data
    }
  }
  getProductos() {
    this.isLoading = true
    this.apiProductos.getProducts(this.params).subscribe(data => {
      this.dataTable = data
      this.isLoading = false
      this.createNotification('success', 'Notificación', 'Se han cargado los registros exitosamente!')
      this.getStore()
    })
  }
  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
  showStock(element:any) {
    this.stock = element.stock
    this.isVisibleStock = true
  }
  refreshStatus() {}
  checkAll(event:any) {
  	console.log(event)
  }
  addToList(data:any) {
    this.showModal = true
    this.selectedFromList = data
    if (data.stock) {
      this.productLimit = 0
      data.stock.map((item:any) => {
        this.selectedFromList['price'] = item.purchase_price
        this.productLimit += item.stock_product
      })
    }
  }
  remove(index:number) {
  	this.selectedProductsList.splice(index, 1)
  	this.selectedProductsList = [
  	...this.selectedProductsList]
  }
  navigateTo() {
    let products = {
      data: this.selectedProductsList
    }
    switch (this.previousUrl) {
      case 2:
        localStorage.setItem('products-compra', JSON.stringify(products))
        break;
      case 1:
        localStorage.setItem('orden-productos', JSON.stringify(products))
        break
      default:
        break;
    }
    this.location.back()
  }
  addDetail() {
    this.showModal = false
    let newItem = {
      ...this.selectedFromList,
      amount: this.selectedAmount
    }
    this.selectedProductsList = [
      ...this.selectedProductsList,
      newItem
    ]
  }
  handleCancel() {
    this.showModal = false
  }
  filterBy(key:any, value:any) {

    value = value.target.value.toString().toUpperCase()

    switch (key) {
      case 'code':
        this.otherFilter['code'] = value
        break;
      case 'supsec':
        this.otherFilter['supsec'] = value
        break;
      case 'brand':
        this.otherFilter['brand.name'] = value
        break;
      case 'model':
        this.dataFilter[0] = {
          "attributeTypeProducts.attribute_type_id": "1",
          "attributeTypeProducts.description": value
        }
        break;
      case 'description':
        this.otherFilter['name'] = value
        break;
      case 'diametro':
        this.dataFilter[2] = {
          "attributeTypeProducts.attribute_type_id": "6",
          "attributeTypeProducts.description": value
          }
        break;
        case 'cmotor':
          this.dataFilter[2] = {
            "attributeTypeProducts.attribute_type_id": "2",
            "attributeTypeProducts.description": value
            }
          break;
      default:
        break
    }
    let filter = this.dataFilter.map((element:any) => {
      if (element["attributeTypeProducts.description"]) {
        return element
      }
    })
    filter = filter.filter((item) => item)
    let params = {
      dataFilter: Object.keys(this.otherFilter).length > 0 ? this.otherFilter: '',
      filterProduct: filter.length > 0 ? filter : ''
    }
    this.apiProductos.filter(params).subscribe(res => {
      this.dataTable = res
    })
  }
  handleCancelStock() {
    this.isVisibleStock = false
  }
  
  handleOkStock() {
    this.isLoading = false
  }

}
