import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  CoinService,
  ExternalService,
  ClientService,
  ProvidersService,
  PaymentService,
  VoucherTypeService,
  AuthenticationService,
  SeriesService,
  ComprasService,
  OperationTypesService,
  OrdenesComprasService
} from '../../../../core/services/index';

@Component({
  selector: 'app-nueva-compra',
  templateUrl: './nueva-compra.component.html',
  styleUrls: ['./nueva-compra.component.scss']
})
export class NuevaCompraComponent implements OnInit {
  
  isLoading: boolean = true;
  isConfirmed: boolean = false;
  showEdit: boolean = false;
  showClient: boolean = false;
  showPayments: boolean = false;
  modalProduct: boolean = false;

  dataTable: any = [];
  productDetails:any = {};
  model: any = {
    observation: 'Compra'
  };
  params: any = {
    dataSearch: ''
  };
  coinList: any = [];
  voucherTypeList: any = [];
  paymentDestinationList: any = [];
  paymentMethodList: any = [];
  selectedClient: any = {};
  selectedSerie: any = {};
  selectedProvider: any = {};
  selectedVoucher: any = {};
  selectedPaymentMethod: any = {};
  selectedDestination: any = {};
  selectedProduct: any = {};
  selectedOperation: any = {};
  numberOfPurchase: number = 0;


  clientList: any = [];
  providerList:any = [];
  productList: any = [];
  operationTypeList: any = [];

  date: Date = new Date();
  serie: number | null = null;
  serie_number: number| null = null;
  total:number = 0;
  exchange_rate: number = 0;
  id: number = 0;
  currentUser: any;
  warehouse_id: number | null = null;
  currentCoin: string = 'S/';

  constructor(
    private apiCoin: CoinService,
    private apiVoucherType: VoucherTypeService,
    private apiPayments: PaymentService,
    private apiProvider: ProvidersService,
    private apiClient: ClientService,
    private apiExternal: ExternalService,
    private auth: AuthenticationService,
    private apiCompra: ComprasService,
    private apiOrden: OrdenesComprasService,
    private apiOperation: OperationTypesService,
    private router: Router,
    private activedRoute: ActivatedRoute
  ) { 
    this.currentUser = this.auth.currentUserValue.user
    this.warehouse_id = this.currentUser.branch_offices[0].pivot.branch_office_id
  }

  ngOnInit(): void {
    let id = parseInt(this.activedRoute.snapshot.paramMap.get('id') || '0', 10)
    this.setStore()
    this.getExchange(this.date)
    this.getCoin()
    this.getClient()
    this.getProviders()
    this.getPaymentDestination()
    this.getPaymentMethod()
    this.getVoucherType()
    this.getOperation()
    if (id) {
      this.getOrderById(id)
    }
  }
  getOrderById(id:number) {
    this.apiOrden.getOne(id).subscribe((data:any) => {
      this.selectedPaymentMethod = this.paymentMethodList.filter((payment:any) => payment.id == data.payment_method_id)[0]
      this.selectedProvider = this.providerList.filter((provider:any) => provider.id == data.provider_id)[0]
      this.dataTable = data.purchase_order_details.map((item:any) => {
        let detail = {
          product_id: item.product_id,
          description: item.product.name,
          amount: item.amount,
          igv: item.igv,
          purchase_price: item.purchase_price,
          price: item.price,
          total: item.purchase_price * item.amount
        }
        this.total += detail.total
        return detail
      })
      // localStorage.setItem('products-compra', JSON.stringify({data: this.dataTable}))
    })
  }
  setStore() {
    let selectedProducts = JSON.parse(localStorage.getItem('products-compra') || '{}') 
    if (selectedProducts.data) {
      this.dataTable = selectedProducts['data'].map((item:any) => {
        let product = {
          warehouse_id: this.warehouse_id,
          product_id: item.id,
          description: item.description,
          price: item.price,
          purchase_price: item.price,
          igv: 12,
          total: item.amount * item.price,
          amount: item.amount
        }
        this.total += product.total
        return product
      })
    }
  }
  getVoucherType() {
    this.apiVoucherType.getAllVouchers().subscribe((res:any) => {
      this.voucherTypeList = res['data']
    })
  }
  getCoin() {
    this.apiCoin.getAll().subscribe((res:any) => {
      this.model['coin'] = res['data'][0]
      this.coinList = res['data']
    })
  }
  getPaymentDestination() {
    this.apiPayments.getDestinations().subscribe((res:any) => {
      this.paymentDestinationList = res['data']
    })
  }
  getPaymentMethod() {
    this.apiPayments.getAll(this.params).subscribe((res:any) => {
      this.paymentMethodList = res['data']
    })
  }
  onSearchCliente(value:any) {

  }
  onSearchProvider(value:any) {

  }
  onSearchProduct(value:string) {

  }
  getCompras() {
    this.apiCompra.getAll().subscribe((res:any) => {
      this.numberOfPurchase = res.length
    })
  }

  getProviders() {
    this.apiProvider.getProviders(this.params).subscribe((data:any) => {
      this.providerList = data
    })
  }
  getClient() {
    this.apiClient.getAll(this.params).subscribe((res:any) => {
      this.clientList = res
    })
  }
  getExchange(date:Date) {
    this.model['created_at'] = date
    this.getExchangeValues(date)
  }
  getExchangeValues (date:any) {
    this.apiExternal.getExchangeValue(date).subscribe((res: any) => {
      
      if (res['exchange_rates'].length > 0 && res['exchange_rates']) {
        let i = res['exchange_rates'].length - 1
        this.exchange_rate = res['exchange_rates'][i]['venta']
      }
    })
  }
  getOperation() {
    this.apiOperation.getAll().subscribe((res:any) => {
      this.operationTypeList = res['data']
    })
  }
  handleOkMiddle() {
    let product = {
      product_id: this.selectedProduct.id,
      description: this.selectedProduct.name,
      amount: this.productDetails.amount.target.value,
      igv: 12,
      purchase_price: parseFloat(this.productDetails.price.target.value),
      price: parseFloat(this.productDetails.price.target.value),
      total: parseFloat(this.productDetails.amount.target.value)*parseFloat(this.productDetails.price.target.value)
    }
    this.total += product['total']
    this.currentCoin = this.model['coin'] ? this.model['coin'].symbol : 'S/'
    this.dataTable = [
      ...this.dataTable,
      product
    ]
    this.modalProduct = false
  }
  
  deleteProduct(index:number) {
    this.total = this.total - this.dataTable[index].total
    this.dataTable.splice(index, 1)
    this.dataTable = [
     ...this.dataTable
    ]
  }
  handleCancelMiddle() {
    this.modalProduct = false
  }
  save() {
    let model = {
      serie: this.serie,
      number: this.serie_number,
      provider_id: this.selectedProvider.id,
      operation_type_id: this.selectedOperation,
      voucher_type_id: this.selectedVoucher,
      exchange_rate: this.exchange_rate,
      igv: "12",
      expiration_date: `${this.model['expiration_date'].getFullYear()}-${this.model['expiration_date'].getMonth()}-${this.model['expiration_date'].getDay()}`,
      coin_id: this.model['coin'].id,
      purchase_details: this.dataTable.map((item:any) => {
        return {
          product_id: item.product_id,
          amount: item.amount,
          warehouse_id: this.currentUser.branch_offices[0].pivot.branch_office_id,
          sale_price: item.price,
          purchase_price: item.price,
          discount: 0,
          igv: item.igv,
          user_created_id: this.currentUser.id
        }
      }),
      purchase_payments: this.selectedPaymentMethod.name ? [
        {
          payment_method_id: this.selectedPaymentMethod.id,
          payment_destination_id: this.selectedDestination.id,
          reference: this.model['reference'],
          user_created_id: this.currentUser.id
        }
      ] : [],
      user_created_id: this.currentUser.id,
      user_updated_id: this.currentUser.id,
      created_at: this.date
    }
    this.apiCompra.add(model).subscribe((res:any) => {
      this.isConfirmed = true
      localStorage.setItem('products-compra', '{}')
    })
  }
  navigateTo(route:string) {
    this.router.navigate([route, 2])
  }
  handleCancelConfirm() {
    this.dataTable = []
    this.total = 0
    this.isConfirmed = false
  }
}
