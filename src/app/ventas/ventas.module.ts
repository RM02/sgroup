import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VentasRoutingModule } from './ventas-routing.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzResultModule } from 'ng-zorro-antd/result';
import { IconsProviderModule } from '../icons-provider.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';


import { ListaComprobantesComponent } from './components/lista-comprobantes/lista-comprobantes.component';
import { NuevoComprobanteComponent } from './components/nuevo-comprobante/nuevo-comprobante.component';
import { VentasComponent } from './ventas.component';
import { ProductosComponent } from './components/productos/productos.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { MarcasComponent } from './components/marcas/marcas.component';
import { CategoriasComponent } from './components/categorias/categorias.component';
import { PedidosComponent } from './components/pedidos/pedidos.component';
import { CotizacionesComponent } from './components/cotizaciones/cotizaciones.component';
import { ComisionesVendedoresComponent } from './components/comisiones-vendedores/comisiones-vendedores.component';
import { ComisionesProductosComponent } from './components/comisiones-productos/comisiones-productos.component';
import { NotaCreditoComponent } from './components/nota-credito/nota-credito.component';
import { NuevoPedidoComponent } from './components/nuevo-pedido/nuevo-pedido.component';
import { NuevaCotizacionComponent } from './components/nueva-cotizacion/nueva-cotizacion.component';
import { SharedComponent } from './components/shared/shared.component';
import { NuevaGuiaComponent } from './components/nueva-guia/nueva-guia.component';

@NgModule({
  declarations: [
    ListaComprobantesComponent,
    NuevoComprobanteComponent,
    VentasComponent,
    ProductosComponent,
    ClientesComponent,
    MarcasComponent,
    CategoriasComponent,
    PedidosComponent,
    CotizacionesComponent,
    ComisionesVendedoresComponent,
    ComisionesProductosComponent,
    NotaCreditoComponent,
    NuevoPedidoComponent,
    NuevaCotizacionComponent,
    SharedComponent,
    NuevaGuiaComponent,
  ],
  imports: [
    CommonModule,
    VentasRoutingModule,
    NzTableModule,
    IconsProviderModule,
    NzCheckboxModule,
    NzLayoutModule,
    NzMenuModule,
    NzNotificationModule,
    NzDividerModule,
    NzButtonModule,
    NzSpaceModule,
    NzGridModule,
    NzInputModule,
    NzCardModule,
    NzSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NzDatePickerModule,
    NzModalModule,
    NzResultModule,
    NzToolTipModule,
    NzInputNumberModule,
    NzAlertModule
  ],
  exports: []
})
export class VentasModule { }
