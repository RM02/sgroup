import { Role } from '../../core/model/role';

export const MenuConfig = [
  {
    label: 'Ventas',
    icon: 'shop',
    roles: [Role.Super_Admin, Role.Admin, Role.Vendedor],
    access: false,
    children: [
      {
        label: 'Nuevo comprobante',
        path: '/ventas/nuevo-comprobante'
      },
      {
        label: 'Listado comprobante',
        path: '/ventas/listado-comprobante'
      },
      {
        label: 'Catalógo',
        children: [
          { label: 'Productos', path: '/ventas/catalogos/productos'},
          { label: 'Categorías', path: '/ventas/catalogos/categorias'},
          { label: 'Marcas', path: '/ventas/catalogos/marcas'},
          { label: 'Clientes', path: '/ventas/catalogos/clientes'}
        ]
      },
      {
        label: 'Pedidos',
        path: '/ventas/pedidos'
      },
      {
        label: 'Cotizaciones',
        path: '/ventas/cotizaciones'
      },
      {
        label: 'Comisiones',
        children: [
          { label: 'Productos', path: '/ventas/comisiones/productos'},
          { label: 'Vendedores', path: '/ventas/comisiones/vendedores'}
        ]
      }       
    ]
  },
  {
    label: 'Compras',
    icon: 'shopping',
    roles: [Role.Super_Admin, Role.Admin, Role.Logístico],
    access: false,
    children: [
      { label: 'Nueva compra', path: '/compras/nueva-compra'},
      { label: 'Listado compras', path: '/compras/listado'},
      { label: 'Ordenes de compra', path: '/compras/ordenes'},
      { label: 'Proveedores', path: '/compras/proveedores'},
    ]
  },
  {
    label: 'Inventario',
    icon: 'container',
    roles: [Role.Super_Admin, Role.Admin, Role.Almacen],
    access: false,
    children: [
      { label: 'Almacen', path: '/inventario/almacen'},
      { label: 'Movimientos', path: '/inventario/movimientos'},
      { label: 'Traslados', path: '/inventario/traslados'},
      { label: 'Devoluciones', path: '/inventario/devoluciones'},
      { label: 'Reporte kardex', path: '/inventario/reporte-kardex'},
      { label: 'Reporte inventario', path: '/inventario/reporte-inventario'},
      // { label: 'Kardex valorizado', path: '/inventario/kardex-valorizado'},
    ]
  },
  {
    label: 'Configuración',
    icon: 'setting',
    roles: [Role.Super_Admin, Role.Admin],
    access: false,
    children: [
      { label: 'Usuarios', path: '/configuracion/usuarios'},
      { label: 'Roles', path: '/configuracion/roles'},
    ]
  },
  {
    label: 'Salir',
    icon: 'logout',
    path: '/auth/login',
    access: true
  }
]