import { Component, OnInit } from '@angular/core';
import { 
  AtributoTypeService,
  ProductoService,
  CategoriasService,
  MarcasService,
  AuthenticationService,
  GeneratorService
} from '../../../core/services';
import { Router } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';

import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime, map, switchMap } from 'rxjs/operators';
// import { Stock } from '../../../core/model/productos.interface';
import { NzNotificationService } from 'ng-zorro-antd/notification';

interface FilterModel {
  "attributeTypeProducts.attribute_type_id": string | null,
  "attributeTypeProducts.description": string | null
}

@Component({
  selector: 'app-shared',
  templateUrl: './shared.component.html',
  styleUrls: ['./shared.component.scss']
})
export class SharedComponent implements OnInit {
  
  searchProductos$ = new BehaviorSubject('');
  searchAttr$ = new BehaviorSubject('');
  
  indeterminate = false;
  showModal: boolean = false;
  stock: any[] = [];
  productLimit: number = 0;
  isVisibleStock: boolean = false;
  showFilter: boolean = true;
  filterLabel:string = "[+ Filtro]"
  dataTable: any = [];
  selectedProductsList:any = [];
  selectedFromList: any = {};
  selectedAmount: number = 1;
  params = {};
  model: any = {
    code: '',
    name: '',
  };
  dataFilter: FilterModel[] = [
    {
      "attributeTypeProducts.attribute_type_id": "1",
      "attributeTypeProducts.description": null
    },
    {
      "attributeTypeProducts.attribute_type_id": "9",
      "attributeTypeProducts.description": null
    },
    {
      "attributeTypeProducts.attribute_type_id": "6",
      "attributeTypeProducts.description": null
    }
  ];
  otherFilter:any = {};
  isLoading: boolean  = false;
  currentUser: any;

  constructor(
    private apiAttr: AtributoTypeService,
    private apiMarcas: MarcasService,
    private apiCategorias: CategoriasService,
    private notification: NzNotificationService,
    private apiProductos: ProductoService,
    private auth: AuthenticationService,
    private router: Router,
    private location: Location,
    private generator: GeneratorService
  ) {
    this.currentUser = this.auth.currentUserValue.user
   }

  ngOnInit(): void {
    this.getProductos()
  }
  getStore() {
    let store = JSON.parse(localStorage.getItem('products-ventas') || '{}')
    if (store.data) {
      this.selectedProductsList = store.data
    }
  }
  getProductos() {
    this.isLoading = true
    this.apiProductos.getProducts(this.params).subscribe(data => {
      this.dataTable = data
      this.isLoading = false
      this.createNotification('success', 'Notificación', 'Se han cargado los registros exitosamente!')
      this.getStore()
    })
  }
  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
  refreshStatus() {}
  checkAll(event:any) {
  	console.log(event)
  }
  addToList(data:any) {
    this.showModal = true
    this.selectedFromList = data
    if (data.stock) {
      this.productLimit = 0
      data.stock.map((item:any) => {
        this.productLimit += item.stock_product
      })
    }
  }
  handleStockModal(data:any) {
    this.stock = data
    this.isVisibleStock = true
  }
  remove(index:number) {
  	this.selectedProductsList.splice(index, 1)
  	this.selectedProductsList = [
  	...this.selectedProductsList]
  }
  navigateTo() {
    let products = {
      data: this.selectedProductsList
    }
    localStorage.setItem('products-ventas', JSON.stringify(products))
    this.location.back()
  }
  addDetail() {
    this.showModal = false
    this.productLimit = 0

    if (this.selectedFromList.stock.length > 0) {
      let newItem = {
        ...this.selectedFromList,
        amount: this.selectedAmount
      }
      this.selectedProductsList = [
        ...this.selectedProductsList,
        newItem
      ]
    }
    this.selectedAmount = 1
  }
  handleCancel() {
    this.showModal = false
    this.productLimit = 0
  }
  filterBy(key:any, value:any) {

    value = value.target.value.toString().toUpperCase()

    switch (key) {
      case 'code':
        this.otherFilter['code'] = value
        break;
      case 'supsec':
        this.otherFilter['supsec'] = value
        break;
      case 'brand':
        this.otherFilter['brand.name'] = value
        break;
      case 'model':
        this.dataFilter[0] = {
          "attributeTypeProducts.attribute_type_id": "1",
          "attributeTypeProducts.description": value
        }
        break;
      case 'description':
        this.otherFilter['name'] = value
        break;
      case 'diametro':
        this.dataFilter[2] = {
          "attributeTypeProducts.attribute_type_id": "6",
          "attributeTypeProducts.description": value
          }
        break;
        case 'cmotor':
          this.dataFilter[2] = {
            "attributeTypeProducts.attribute_type_id": "2",
            "attributeTypeProducts.description": value
            }
          break;
      default:
        break
    }
    let filter = this.dataFilter.map((element:any) => {
      if (element["attributeTypeProducts.description"]) {
        return element
      }
    })
    filter = filter.filter((item) => item)
    let params = {
      dataFilter: Object.keys(this.otherFilter).length > 0 ? this.otherFilter: '',
      filterProduct: filter.length > 0 ? filter : ''
    }
    this.apiProductos.filter(params).subscribe(res => {
      this.dataTable = res
    })
  }
  handleCancelStock() {
    this.isVisibleStock = false
  }
  handleOkStock() {
    this.isVisibleStock = false
  }

}
