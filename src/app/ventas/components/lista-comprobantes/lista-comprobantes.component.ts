import { Component, OnInit } from '@angular/core';
import { ComprobanteService, GeneratorService, ClientService, GuiasService } from '../../../core/services/index';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-comprobantes',
  templateUrl: './lista-comprobantes.component.html',
  styleUrls: ['./lista-comprobantes.component.scss']
})
export class ListaComprobantesComponent implements OnInit {
  
  isLoading: boolean = false;
  export: boolean = false;
  showFiltro: boolean = false;
  showFilter: boolean = false;
  
  params: any = {};
  search: any = {};
  date: Date | null = null;
  clientList: any = [];
  selectedGuide: any = {};
  guiaList: any = [];
  dataTable: any[] = [];
  guideDescription: string = '';
  guideObservation: string = '';
  selectedRegister: any = {};

  constructor(
    private router: Router,
    private guias: GuiasService,
    private apiClient: ClientService,
    private generator: GeneratorService,
    private apiComprobantes: ComprobanteService,
    private notification: NzNotificationService 
  ) { }

  ngOnInit() {
    this.getComprobantes()
    this.getClients()
    this.getGuias()
  }
  showModalDetails(data:any) {
    this.router.navigate(['/ventas/nueva-guia', data.id])
  }
  getGuias() {
    this.guias.getAll().subscribe((res:any) => {
      this.guiaList = res['data']
    })
  }
  getClients() {
    this.apiClient.getAll({}).subscribe((res:any) => {
      this.clientList = res
    })
  }
  addNote(id:number) {
    this.router.navigate(['ventas', 'nota-credito', id])
  }
  getComprobantes() {
    this.isLoading = true
    this.apiComprobantes.getAll(this.params).subscribe((res) => {
      this.dataTable = res
      this.isLoading = false
      this.createNotification('success', 'Notificación', 'Se han cargado los registros exitosamente!')
    })
  }
  openFiltro() {
    this.showFiltro = !this.showFiltro
  }
  filterBy(key:string, value:any) {
    if(key == 'created_at') {
      value = this.date
    } else {
      value = value.target.value.toString()
    }
    let dataFilter = {
      [key]: value
    }
    this.params['dataSearch'] = dataFilter

    this.apiComprobantes.getAll(this.params).subscribe((res: any) => {
      this.dataTable = res
      this.isLoading = false
    })
  }
  /**
  * Return products attributes
  */
  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
   getAttributes(attributes:any[], obj:any) {
    attributes.map(element => {
      obj[element.name] = element.pivot.description
    });
    return obj
  }
  delete(id:number) {
    this.apiComprobantes.delete(id).subscribe((res) => {
      this.getComprobantes()
    })
  }
  navigateTo(route:string) {
    this.router.navigate(['ventas', route])
  }
  downloadPdf() {
    let pdfContent = this.createExcelForm(this.dataTable)
    let data = {
      header: "REPUESTOS | GRUPO SUDAMÉRICA",
      pageSize: 'A3',
      fontSize: 10,
      content: pdfContent
    }
    this.generator.generatePDF(data)
  } 
  downloadExcel() {
    let boletas = this.createExcelForm(this.dataTable, 'BOLETA DE VENTA ELECTRÓNICA')
    let facturas = this.createExcelForm(this.dataTable, 'FACTURA ELECTRÓNICA')
	  let creditos = this.createExcelForm(this.dataTable, 'NOTA DE CRÉDITO')
    this.generator.exportExcel([boletas, facturas, creditos], 'registro_ventas')
    this.export = false
  }
  createExcelForm(data:any, type_doc?: string) {

    var excelData = [['Item', 'Código', 'Nombre o razón social', 'N° R.U.C', 'Tipo Doc.', 'N° Documento', 'Fecha Emisión', 'Fecha Vcto.', 'Mon', '%', 'Importe S/', 'Importe US$', 'T. Cam.', 'B. Importe', 'Otros cargos', 'IGV', 'Total']]
      for (let index = 0; index < data.length; index++) {
        if (type_doc) {
          let doc = data[index]['voucher_type']['name'].toUpperCase()
          if (doc == type_doc) {
            let client = this.clientList.filter((client:any) => client.id == data[index]['client_id'])[0]
            let valor_importe = data[index]['total'] / data[index]['exchange_rate']
            let bImponible = (data[index]['total'] + (data[index]['total']*data[index]['igv'])/100).toFixed(2)
            let element = [index + 1, 0, data[index]['cliente'], client['document_number'], data[index]['voucher_type']['name'], data[index]['number'], data[index]['created_at'], data[index]['expiration_date'], data[index]['coinName'], " ", " ", valor_importe, data[index]['exchange_rate'], bImponible, " ", data[index]['igv'], data[index]['total']];
            excelData.push(element)
          }
        }
        else {
          let client = this.clientList.filter((client:any) => client.id == data[index]['client_id'])[0]
          let valor_importe = data[index]['total'] / data[index]['exchange_rate']
          let bImponible = (data[index]['total'] + (data[index]['total']*data[index]['igv'])/100).toFixed(2)
          let element = [index + 1, 0, data[index]['cliente'], client['document_number'], data[index]['voucher_type']['name'], data[index]['number'], data[index]['created_at'], data[index]['expiration_date'], data[index]['coinName'], " ", " ", valor_importe, data[index]['exchange_rate'], bImponible, " ", data[index]['igv'], data[index]['total']];
          excelData.push(element)
        }
      }
      return excelData
    }
    onBtnFilter() {
      this.showFilter = !this.showFilter
    }
}
