import { Component, OnInit } from '@angular/core';
import { 
  AtributoTypeService,
  ProductoService,
  CategoriasService,
  MarcasService,
  AuthenticationService,
  GeneratorService
} from '../../../core/services';

import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime, map, switchMap } from 'rxjs/operators';
import { Stock } from '../../../core/model/productos.interface';
import { NzNotificationService } from 'ng-zorro-antd/notification';


interface FilterModel {
  "attributeTypeProducts.attribute_type_id": string | null,
  "attributeTypeProducts.description": string | null
}

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {

  searchProductos$ = new BehaviorSubject('');
  searchAttr$ = new BehaviorSubject('');

  titleModal: string = '';
  numberOfDivAttr: any = [1];
  selectedAttrList: any = []
  stock: Stock[] = [];
  showEdit: boolean = false;
  isVisibleModalEditAdd: boolean = false;
  isVisibleStock: boolean = false;
  export: boolean = false;
  showFilter: boolean = true;
  filterLabel:string = "[+ Filtro]"
  selectedAttr: any = [];
  dataTable: any = [];
  attrList: any = [];
  marcasList: any = [];
  selectedMarca: any = [];
  selectedCategoria: any = [];
  categoriaList: any = [];
  params = {};
  model: any = {
    code: '',
    name: '',
  };
  dataFilter: FilterModel[] = [
    {
      "attributeTypeProducts.attribute_type_id": "1",
      "attributeTypeProducts.description": null
    },
    {
      "attributeTypeProducts.attribute_type_id": "9",
      "attributeTypeProducts.description": null
    },
    {
      "attributeTypeProducts.attribute_type_id": "6",
      "attributeTypeProducts.description": null
    }
  ];
  otherFilter:any = {};
  isLoading: boolean  = false;
  currentUser: any;

  constructor(
    private apiAttr: AtributoTypeService,
    private apiMarcas: MarcasService,
    private apiCategorias: CategoriasService,
    private notification: NzNotificationService,
    private apiProductos: ProductoService,
    private auth: AuthenticationService,
    private generator: GeneratorService
  ) {
    this.currentUser = this.auth.currentUserValue.user
   }

  ngOnInit() {
    this.getProductos()
    this.getAllAttr()
    this.getMarcas()
    this.getCategorias()
  }
  getProductos() {
    this.isLoading = true
    this.apiProductos.getProducts(this.params)
    .subscribe(
      data => {
        this.dataTable = data
        this.isLoading = false
        this.createNotification('success', 'Notificación', 'Se han cargado los registros exitosamente!')
      },
      e => {
        console.log(e)
        this.createNotification('warning', 'Notificación', 'Error con la conexión al servidor')
      }
    )
  }
  filterBy(key:any, value:any) {

    value = value.target.value.toString().toUpperCase()

    switch (key) {
      case 'code':
        this.otherFilter['code'] = value
        break;
      case 'supsec':
        this.otherFilter['supsec'] = value
        break;
      case 'brand':
        this.otherFilter['brand.name'] = value
        break;
      case 'model':
        this.dataFilter[0] = {
          "attributeTypeProducts.attribute_type_id": "1",
          "attributeTypeProducts.description": value
        }
        break;
      case 'description':
        this.otherFilter['name'] = value
        break;
      case 'diametro':
        this.dataFilter[2] = {
          "attributeTypeProducts.attribute_type_id": "6",
          "attributeTypeProducts.description": value
          }
        break;
        case 'cmotor':
          this.dataFilter[2] = {
            "attributeTypeProducts.attribute_type_id": "2",
            "attributeTypeProducts.description": value
            }
          break;
      default:
        break
    }
    let filter = this.dataFilter.map((element:any) => {
      if (element["attributeTypeProducts.description"]) {
        return element
      }
    })
    filter = filter.filter((item) => item)
    let params = {
      dataFilter: Object.keys(this.otherFilter).length > 0 ? this.otherFilter: '',
      filterProduct: filter.length > 0 ? filter : ''
    }
    this.apiProductos.filter(params).subscribe(res => {
      this.dataTable = res
    })
  }
  visibleFilter() {
    this.showFilter = !this.showFilter
    if(this.showFilter) {
      this.filterLabel = "[-Filtro]"
    } else {
      this.filterLabel = "[+Filtro]"
    }
  }
  getAllAttr() {
    const getAttrList = (obj:any) =>
    this.apiAttr.getAttributes()
    .pipe(map((res: any) => res.data))
    .pipe(
      map((list: any) => {
        return list.map((item: any) => item)
      })
    );
    const attrList$: Observable<any> = this.searchAttr$
    .asObservable()
    .pipe(debounceTime(500))
    .pipe(switchMap(getAttrList));
    attrList$.subscribe(data => {
      this.attrList = data
    })
  }
  getMarcas() {
    this.apiMarcas.getAll(this.params)
    .subscribe((res:any) => {
      this.marcasList = res['data']
    })
  } 
  onSearchMarca(value: any): void {
    this.params = {
      dataSearch: {
        name: value.toUpperCase()
      }
    }
    debounceTime(100)
    this.getMarcas()
  }
  getCategorias() {
    this.apiCategorias.getAll(this.params)
    .subscribe((res:any) => {
      this.categoriaList = res['data']
    })
  }
  onSearchCategoria(value: any): void {
    this.params = {
      dataSearch: {
        name: value.toUpperCase()
      }
    }
    debounceTime(100)
    this.getCategorias()
  }
  delete(id:number) {
    this.apiProductos.delete(id).subscribe((res) => {
      this.getProductos()
    })
  }
  showModalEdit(data:any) {
    this.titleModal = 'Editar producto'
    this.model = {
      ...data
    }
    this.selectedMarca = data.brand
    this.selectedCategoria = data.category
    this.isVisibleModalEditAdd = true
  }
  showStock(element:any) {
    this.stock = element.stock
    this.isVisibleStock = true
  }
  handleOkStock(): void {
    this.isVisibleStock = false;
  }

  handleCancelStock(): void {
    this.isVisibleStock = false;
  }
  handleCancel() {
    this.isVisibleModalEditAdd = false
  }
  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
  onSelectedAttr(attr:any) {
    this.selectedAttrList.push(attr)
  }
  removeAttr(index:number) {
    this.numberOfDivAttr.splice(index, 1)
    this.selectedAttrList.splice(index, 1)
  }
  setValue(index: number, data:any) {
    this.selectedAttrList[index]['value'] = data.target.value
  }
  showAddModal() {
    this.titleModal = 'Agregar producto'
    this.model = {}
    this.isVisibleModalEditAdd = true
  }
  addProduct() {
    let attr = this.selectedAttrList.map((item:any) => {
      return {
        attribute_type_id: item.id,
        description: item.value,
        user_created_id: this.currentUser.id
      }
    })
    let product = {
      ...this.model,
      attribute_types: attr,
      description: this.model['name'],
      brand_id:  this.selectedMarca.id,
      category_id: this.selectedCategoria.id,
      user_created_id: this.currentUser.id,
      user_updated_id: this.currentUser.id
    }
    this.apiProductos.addProduct(product).subscribe((res) => {
      this.getProductos()
      this.isVisibleModalEditAdd = false
    })
  }
  
  downloadPdf() {
    var pdfContent = [['ITEM', 'MARVEH', 'CODPRO', 'SUPSEC', 'NOMBRE', 'NUMSEC', 'DESCRIPCIÓN', 'UM', 'CATEGORÍA', 'MODELO', 'CMOTOR', 'ALTERNANTE', 'CILINDROS', 'DIAMETRO', 'NUMCIL']]
    var n = 0
    this.dataTable.map((element:any) => {
      n += 1
      if (element) {
        pdfContent.push([n, element.brand ? element.brand: " ", element.code ? element.code: " ", element.supsec ? element.supsec : " ", element.name ? element.name: " ", element.numsec ? element.numsec : " ", element.description ? element.description: " ", element.um ? element.um: " ", element.category ? element.category: " ", element.modelo ? element.modelo: " ", element.cmotor ? element.cmotor: " ", element.alternante ? element.alternante: " ", element.cilindros ? element.cilindros: " ", element.diametro ? element.diametro: " ", element.numcil ? element.numcil: " "])
      }
    })    
    let data = {
      header: "REPUESTOS | GRUPO SUDAMÉRICA",
      pageSize: 'A3',
      fontSize: 10,
      content: pdfContent
    }
    this.generator.generatePDF(data)
  }
  onScroll() {

  }
}
