import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  ComprobanteService,
  ProductoService,
  AuthenticationService,
  CreditNoteTypesService,
  VoucherTypeService
} from '../../../core/services/index';

@Component({
  selector: 'app-nota-credito',
  templateUrl: './nota-credito.component.html',
  styleUrls: ['./nota-credito.component.scss']
})
export class NotaCreditoComponent implements OnInit {

  bill: any = {};
  details: any = {};
  productDetails: any = [];
  dataTable: any = [];

  isVisibleMiddle: boolean = false;
  params: any = {};
  selectedProduct: any = {};
  noteList: any = [];
  productList: any = [];
  voucherList: any = [];
  currentUser: any;
  
  constructor(
    private auth: AuthenticationService,
    private apiProduct: ProductoService,
    private apiTypeOfNote: CreditNoteTypesService,
    private apiComprobante: ComprobanteService,
    private apiVoucherType: VoucherTypeService,
    private route: ActivatedRoute
  ) {
    this.currentUser = this.auth.currentUserValue.user
  }

  ngOnInit(): void {
    let id = parseInt(this.route.snapshot.paramMap.get('id') || '0', 10)
    this.getComprobante(id)
    this.getNotes()
    this.getVouchers()
    this.getProducts()
  }
  onSearchProduct(value:string) {

  }
  getProducts() {
    this.apiProduct.getProducts(this.params).subscribe((res:any) => {
      this.productList = res
    })
  }
  getVouchers() {
    this.apiVoucherType.getAllVouchers().subscribe((res:any) => {
      this.voucherList = res['data']
    })
  }
  getComprobante(id:number) {
    this.apiComprobante.getOne(id).subscribe((res:any) => {
      [res].map((item:any) => {
        item.client = item.client.name
        item.coin = item.coin.name
        item.seller = item.seller.name
      })
      this.bill = {...res}
      this.dataTable = this.bill['bill_electronic_details']
    })
  }
  getNotes() {
    this.apiTypeOfNote.getAll().subscribe((res:any) => {
      this.noteList = res['data']
    })
  }
  addNote() {
    let model = {
      seller_id: this.bill.seller_id,
      voucher_type_note_id: this.details['voucher_type_note_id'],
      type_of_credit_note_id: this.details['type_of_credit_note_id'],
      description: this.details['description'],
      purchase_order: this.details['purchase_order'],
      exchange_rate: this.bill['exchange_rate'],
      credit_note_details: this.dataTable.map((item:any) => {
        return {
          product_id: item.product_id,
          amount: item.amount,
          price: item.price,
          igv: item.igv,
          purchase_price: item.purchase_price,
          user_created_id: item.user_created_id
        }
      }),
      user_created_id: this.currentUser.id,
      user_updated_id: this.currentUser.id
    }
    /*this.apiComprobante.note(id, model).subscribe(() => {

    })*/
  }
  handleOkMiddle() {

  }
}
