import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { 
  AuthenticationService,
  ClientService,
  ExternalService,
  PaymentService,
  CoinService,
  SellersService,
  ProductoService,
  CotizacionesService
} from '../../../core/services/index';

@Component({
  selector: 'app-nueva-cotizacion',
  templateUrl: './nueva-cotizacion.component.html',
  styleUrls: ['./nueva-cotizacion.component.scss']
})
export class NuevaCotizacionComponent implements OnInit {

  isVisibleMiddle: boolean = false;
  isConfirmed: boolean = false;
  productList: any = [];
  selectedProduct: any = {};
  productDetails: any = {};

  currentUser: any  = {};
  clientList: any = [];
  sellerList: any = [];
  selectedUser: any = {};
  paymentMethodList: any = [];
  paymentDestinationList: any = [];
  params:any = {};
  dataTable: any = [];
  coinList: any = [];
  model: any = {};
  total: number = 0;
  currentCoin: string = '';
  date: Date = new Date();
  exchange_rate: number = 0;

  constructor(
    private apiSeller: SellersService,
    private apiExternal: ExternalService,
    private apiCoin: CoinService,
    private router: Router,
    private apiCotizaciones: CotizacionesService,
    private apiProdcut: ProductoService,
    private apiClient: ClientService,
    private apiPayment: PaymentService,
    private auth: AuthenticationService
  ) {
    this.currentUser = this.auth.currentUserValue.user
   }

  ngOnInit(): void {
    this.getClient()
    this.getExchange(this.date)
    this.getPaymentMethods()
    this.getPaymentDestination()
    this.getCoin()
    this.getSellers()
    this.getProducts()
  }
  
  getClient() {
    this.apiClient.getAll(this.params)
    .subscribe((res) => {
      this.clientList = res
    })
  }
  getSellers() {
    this.apiSeller.getAll(this.params).subscribe((res:any) => {
      this.sellerList = res['data']
    })
  }
  getProducts() {
    this.apiProdcut.getProducts(this.params)
    .subscribe((res:any) => {
      this.productList = res
    })
  }
  onSearchCliente (value:string) {
    this.params = {
      dataSearch: {
        name: value
      }
    }
    this.getClient()
  }
  
  getPaymentDestination() {
    this.apiPayment.getDestinations()
    .subscribe((res:any) => {
      this.paymentDestinationList = res['data']
    })
  }
  
  getPaymentMethods() {
    this.apiPayment.getAll(this.params)
    .subscribe((res:any) => {
      this.paymentMethodList = res['data']
    })
  }
  navigateTo() {
    this.router.navigate(['ventas', 'cotizaciones'])
  }
  handleCancelConfirm() {
    this.isConfirmed = false
  }
  getCoin() {
    this.apiCoin.getAll()
    .subscribe((res:any) => {
      this.coinList = res['data']
    })
  }
  getExchange(date:Date) {
    this.model['created_at'] = date
    this.getExchangeValues(date)
  }
  getExchangeValues (date:any) {
    this.apiExternal.getExchangeValue(date).subscribe((res: any) => {
      
      if (res['exchange_rates'].length > 0 && res['exchange_rates']) {
        let i = res['exchange_rates'].length - 1
        this.exchange_rate = res['exchange_rates'][i]['venta']
      }
    })
  }
  handleCancelMiddle() {
    this.isVisibleMiddle = false
  }
  handleOkMiddle(): void {
    let product = {
      product_id: this.selectedProduct.id,
      description: this.selectedProduct.name,
      amount: this.productDetails.amount.target.value,
      igv: 12,
      purchase_price: parseFloat(this.productDetails.price.target.value),
      price: parseFloat(this.productDetails.price.target.value),
      total: parseFloat(this.productDetails.amount.target.value)*parseFloat(this.productDetails.price.target.value)
    }
    this.total += product['total']
    this.currentCoin = this.model['coin'] ? this.model['coin'].symbol : 'S/' 
    this.dataTable = [
      ...this.dataTable,
      product
    ]
    this.isVisibleMiddle = false;
  }
  deleteProduct(index:number) {

  }
  onSearchProduct(value:string) {
    this.params = {
      dataSearch: {
        name: value.toUpperCase()
      }
    }
    debounceTime(100)
    this.getProducts()
  }
  save() {
    let cotizacion = {
      client_id: this.selectedUser.id,
      seller_id: this.model['seller_id'],
      coin_id: this.model['coin'].id,
      exchange_rate: this.exchange_rate,
      igv: "12",
      expiration_date: this.date,
      validity_time: this.model['validity_time'],
      delivery_time: this.model['delivery_time'],
      shipping_address: this.model['shipping_address'],
      payment_method_id: this.model['payment_method_id'],
      account_number: this.model['account_number'],
      quotation_details: this.dataTable.map((item:any) => {
        return {
          product_id: item.product_id,
          amount: item.amount,
          price: item.price,
          igv: item.igv,
          purchase_price: item.purchase_price,
          extra_attribute: 'N/A',
          extra_attribute_description: 'N/A',
          user_created_id: this.currentUser.id
        }
      }),
      quotation_payments: [{
        payment_method_id: this.model['payment_method_id'],
        payment_destination_id: this.model['payment_destination_id'],
        amount: this.total,
        user_created_id: this.currentUser.id,
        reference: 'N/A'
      }],
      user_created_id: this.currentUser.id,
      user_updated_id: this.currentUser.id

    }
    this.apiCotizaciones.create(cotizacion).subscribe((res:any) => {
      this.isConfirmed = true
    })
  }
}
