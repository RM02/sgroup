import { Component, OnInit } from '@angular/core';
import { 
  AuthenticationService,
  ClientService,
  CoinService,
  SellersService,
  ExternalService,
  PaymentService,
  ProductoService,
  PedidoService
} from '../../../core/services/index';
import { debounceTime, map, switchMap } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nuevo-pedido',
  templateUrl: './nuevo-pedido.component.html',
  styleUrls: ['./nuevo-pedido.component.scss']
})
export class NuevoPedidoComponent implements OnInit {

  currentUser: any = {};
  searchProduct$ = new BehaviorSubject('');
  isVisibleMiddle: boolean = false;
  isConfirmed: boolean = false;
  selectedProduct: any = {};
  productDetails: any = {};
  productList: any = [];

  clientList: any = [];
  coinList: any = [];
  sellerList: any = [];
  paymentMethodList: any = [];
  exchange_rate: number = 0;

  selectedUser: any = {};
  date: Date = new Date();
  model: any = {};
  total: number = 0;
  currentCoin: string = 'S/';
  params:any = {};

  dataTable: any = [];

  constructor(
    private auth: AuthenticationService,
    private apiClient: ClientService,
    private apiCoin: CoinService,
    private apiPayment: PaymentService,
    private apiProductos: ProductoService,
    private apiSeller: SellersService,
    private apiExternal: ExternalService,
    private apiPedido: PedidoService,
    private router: Router
  ) {
    this.currentUser = this.auth.currentUserValue.user
   }

  ngOnInit(): void {
    this.getClient()
    this.getCoin()
    this.getSeller()
    this.getProducto()
    this.getPaymentMethods()
    this.getExchange(this.date)
  }
  
  getClient() {
    this.apiClient.getAll(this.params)
    .subscribe((res) => {
      this.clientList = res
    })
  }
  getCoin() {
    this.apiCoin.getAll()
    .subscribe((res:any) => {
      this.coinList = res['data']
    })
  }
  
  getSeller() {
    this.apiSeller.getAll(this.params)
    .subscribe((res:any) => {
      this.sellerList = res['data']
    })
  }
  
  getExchange(date:Date) {
    this.model['created_at'] = date
    this.getExchangeValues(date)
  }
  getExchangeValues (date:any) {
    this.apiExternal.getExchangeValue(date).subscribe((res: any) => {
      
      if (res['exchange_rates'].length > 0 && res['exchange_rates']) {
        let i = res['exchange_rates'].length - 1
        this.exchange_rate = res['exchange_rates'][i]['venta']
      }
    })
  }

  onSearchCliente(value:any) {
    this.params = {
      dataSearch: {
        name: value.toUpperCase()
      }
    }
    debounceTime(100)
    this.getClient()
  }
  deleteProduct(index:number) {
    this.dataTable.splice(index, 1)
    this.dataTable = [
     ...this.dataTable
    ]
  }
  save() {
    let pedido = {
      client_id: this.selectedUser.id,
      seller_id: this.model['seller_id'],
      coin_id: this.model['coin'].id,
      address: this.model['address'],
      observation: this.model['observation'],
      payment_method_id: this.model['payment_method_id'],
      created_at: this.date,
      delivery_date: this.model['delivery_date'],
      expiration_date: this.model['expiration_date'],
      exchange_rate: this.exchange_rate,
      order_details: this.dataTable.map((item:any) => {
        return {
          product_id: item.product_id,
          amount: item.amount,
          user_created_id: this.currentUser.id,
          price: item.price,
          purchase_price: item.total,
          igv: item.igv
        }
      }),
      user_created_id: this.currentUser.id,
      user_updated_id: this.currentUser.id
    }
    this.apiPedido.post(pedido).subscribe((res:any) => {
      this.isConfirmed = true
    })
  }
  navigateTo() {
    this.router.navigate(['ventas/pedidos'])
  }
  
  getProducto() {
    const getProductoList = (name: string) =>
    this.apiProductos.getProducts(this.params)
    .pipe(map((res: any) => res))
    .pipe(
      map((list: any) => {
        return list.map((item: any) => item)
      })
    );
    const productList$: Observable<string[]> = this.searchProduct$
    .asObservable()
    .pipe(debounceTime(500))
    .pipe(switchMap(getProductoList));
    productList$.subscribe(data => {
      this.productList = data
    })
  }
  
  handleOkMiddle(): void {
    let product = {
      product_id: this.selectedProduct.id,
      description: this.selectedProduct.name,
      amount: parseFloat(this.productDetails.amount.target.value),
      igv: 12,
      purchase_price: parseFloat(this.productDetails.price.target.value),
      price: parseFloat(this.productDetails.price.target.value),
      total: parseFloat(this.productDetails.amount.target.value)*parseFloat(this.productDetails.price.target.value)
    }
    this.total += product['total']
    this.currentCoin = this.model['coin'] ? this.model['coin'].symbol : this.currentCoin, 
    this.dataTable = [
      ...this.dataTable,
      product
    ]
    this.isVisibleMiddle = false;
  }
  handleCancelConfirm () {
    this.isConfirmed = false
  }
  handleCancelMiddle(): void {
    this.isVisibleMiddle = false;
  }
  onSearchProduct(value: string): void {
    this.params = {
      dataSearch: {
        name: value.toUpperCase()
      }
    }
    this.searchProduct$.next(value);
  }
  
  getPaymentMethods() {
    this.apiPayment.getAll(this.params)
    .subscribe((res:any) => {
      this.paymentMethodList = res['data']
    })
  }

}
