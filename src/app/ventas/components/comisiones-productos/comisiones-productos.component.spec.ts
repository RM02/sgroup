import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComisionesProductosComponent } from './comisiones-productos.component';

describe('ComisionesProductosComponent', () => {
  let component: ComisionesProductosComponent;
  let fixture: ComponentFixture<ComisionesProductosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComisionesProductosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComisionesProductosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
