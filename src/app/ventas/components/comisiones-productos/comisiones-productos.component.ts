import { Component, OnInit } from '@angular/core';
import { ComisionesProductosService, AuthenticationService, ProductoService, GeneratorService } from '../../../core/services/index';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-comisiones-productos',
  templateUrl: './comisiones-productos.component.html',
  styleUrls: ['./comisiones-productos.component.scss']
})
export class ComisionesProductosComponent implements OnInit {
  
  dataTable:any = [];
  isLoading: boolean = false;
  export: boolean = false;
  isVisibleModal: boolean = false;
  titleModal: string = '';

  product_id: number = 0;
  commission_type: string = '';
  amount: string = '';
  product_name: string = '';

  selectedComision: any;
  selectedProduct: any = [];
  currentUser:any;
  params: any = {}
  productList: any = [];
  comisionTypes: string[] = ['Monto', 'Porcentaje']
  
  constructor(
    private apiComisiones: ComisionesProductosService,
    private apiProductos: ProductoService,
    private auth: AuthenticationService,
    private generator: GeneratorService,
    private notification: NzNotificationService 
    ) {
      this.currentUser = this.auth.currentUserValue.user
     }

  ngOnInit(): void {
    this.getComisiones()
    this.getProductos()
  }
  getComisiones() {
    this.isLoading = true
    this.apiComisiones.getAll().subscribe((res:any) => {
      this.dataTable = res['data']
      this.isLoading = false
      this.createNotification('success', 'Notificación', 'Se han cargado los registros exitosamente!')
    })
  }
  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
  showModal (data:any) {
    this.product_id = data.product_id
    this.product_name = data.product.name
    this.amount = data.amount
    this.selectedComision = data.commission_type
    this.titleModal = 'Editar comisión'
    this.isVisibleModal = true
  }
  handleCancel() {
    this.isVisibleModal = false
  }
  handleOk() {
      let model = {
        product_id: this.product_id,
        commission_type: this.selectedComision,
        amount: this.amount,
        user_created_id: this.currentUser.id,
        user_updated_id: this.currentUser.id
      }
    if (this.titleModal == 'Editar comisión') {

      this.apiComisiones.update(model.product_id, model).subscribe((res) => {
        this.isVisibleModal = false
        this.getComisiones()
      })
    }
    else if (this.titleModal == 'Agregar nueva comisión') {
      model['product_id'] = this.selectedProduct
      this.apiComisiones.post(model).subscribe((res) => {
        this.isVisibleModal = false
        this.getComisiones()
      })
    }
  }
  onSelectedComision(value:any) {
    console.log(value)
  }
  showAddModal() {
    this.titleModal = 'Agregar nueva comisión'
    this.commission_type = '',
    this.amount = '',
    this.selectedComision = ''
    this.isVisibleModal = true
  }
  getProductos() {
    this.apiProductos.getProducts(this.params).subscribe((res:any) => {
      this.productList = res
    })
  }
  onSearch(value: any) {
    this.params = {
      dataSearch: {
        name: value.toUpperCase()
      }
    }
    debounceTime(100)
    this.getProductos()
  }
  delete(id:number) {
    this.apiComisiones.delete(id).subscribe((res) => {
      this.getComisiones()
    })
  }
  
  downloadPdf() {
    let content = this.pdfExcelModel()
    let pdf = {
      header: "REPUESTOS | GRUPO SUDAMÉRICA",
      pageSize: 'A4',
      fontSize: 10,
      width: "*",
      orientation: "portrait",
      content: content
    }
    this.generator.generatePDF(pdf)
  }
  downloadExcel() {
    let content = this.pdfExcelModel()
    this.generator.exportExcel(content, "Registro comisiones productos")
    this.export = false
  }
  pdfExcelModel() {
    let model = [['Item', 'Producto', 'Tipo comisión', 'Comisión']]
    let n = 0
    this.dataTable.map((item:any) => {
      n += 1
      model.push([n, item.product.name, item.commission_type, item.amount])
    })
    return model
  }
}
