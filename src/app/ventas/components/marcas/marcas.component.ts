import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { NzNotificationService } from 'ng-zorro-antd/notification';
import { MarcasService, GeneratorService, AuthenticationService } from '../../../core/services/index';

@Component({
  selector: 'app-marcas',
  templateUrl: './marcas.component.html',
  styleUrls: ['./marcas.component.scss']
})
export class MarcasComponent implements OnInit {

  searchMarcas$ = new BehaviorSubject('');
  
  isLoading: boolean = false;
  showFilter: boolean = false;
  export: boolean = false;
  isVisibleModal: boolean = false;
  modalTitle: string = "Agregar marca";
  dataTable: any = [];
  params: any = {};
  model:any = {};
  currentUser: any = {};

  constructor(
    private generator: GeneratorService,
    private auth: AuthenticationService,
    private apiMarcas: MarcasService,
    private notification: NzNotificationService,
  ) {
    this.currentUser = this.auth.currentUserValue.user
  }

  ngOnInit(): void {
    this.getMarcas()
  }
  getMarcas() {
    this.isLoading = true
    this.apiMarcas.getAll(this.params)
    .subscribe((res:any) => {
      this.dataTable = res['data']
      this.isLoading = false
      this.createNotification('success', 'Notificación', 'Se han cargado los registros exitosamente!')
    })
  }

  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
  downloadPdf() {
    let content = this.pdfExcelModel()
    let pdf = {
      header: "REPUESTOS | GRUPO SUDAMÉRICA",
      pageSize: 'A4',
      fontSize: 10,
      width: "*",
      orientation: "portrait",
      content: content
    }
    this.generator.generatePDF(pdf)
  }
  downloadExcel() {
    let content = this.pdfExcelModel()
    this.generator.exportExcel(content, "Registro marcas")
    this.export = false
  }
  pdfExcelModel() {
    let model = [['Item', 'Nombre', 'Descripción']]
    let n = 0
    this.dataTable.map((item:any) => {
      n += 1
      model.push([n, item.name, item.description])
    })
    return model
  }
  showModalAddEdit(section:string, data?:any) {
    if (section == "add") {
      this.model = {}
      this.modalTitle = "Agregar marca"
    } else {
      this.model = {
        ...data
      }
      this.modalTitle = "Editar marca"
    }
    this.isVisibleModal = true
  }
  
  handleCancel() {
    let categoria = {
      ...this.model,
      user_created_id: this.currentUser.id,
      user_updated_id: this.currentUser.id
    }
    this.isVisibleModal = false
  }
  handleOk() {
    let marca = {
      ...this.model,
      user_created_id: this.currentUser.id,
      user_updated_id: this.currentUser.id
    }
    if (this.modalTitle == "Agregar categoría") {
      this.addMarcas(marca)
    } else {
      this.edit(marca)
    }
  }
  edit(model:any) {
    this.apiMarcas.updateBrand(model).subscribe((res:any) => {
      this.getMarcas()
      this.isVisibleModal = false
    })
  }
  addMarcas(model:any) {
    this.apiMarcas.addBrand(model).subscribe((res:any) => {
      this.getMarcas()
      this.isVisibleModal = false
    })
  }
  delete(id:number) {
    this.apiMarcas.delete(id).subscribe((res:any) => {
      this.getMarcas()
    })
  }
  
  filterBy(key:string, value:any) {
    value = value.target.value.toString().toUpperCase()
    let params = {
      dataFilter: {
        [key]: value
      }
    }
    this.apiMarcas.getAll(params).subscribe((res:any) => {
      this.dataTable = res['data']
    })
  }
  visibleFilter() {
    this.showFilter = !this.showFilter
  }
}
