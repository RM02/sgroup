import { Component, OnInit } from '@angular/core';
import { 
  CategoriasService, 
  AuthenticationService,
  GeneratorService
} from '../../../core/services/index';
import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime, map, switchMap } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.scss']
})
export class CategoriasComponent implements OnInit {
  
  searchCategorias$ = new BehaviorSubject('');

  isLoading: boolean = false;
  showFilter: boolean = false;
  export: boolean = false;
  isVisibleModal: boolean = false;
  modalTitle: string = "Agregar categoría";
  currentUser: any = {};
  dataTable: any = [];
  model: any = {};
  params:any = {};

  constructor(
    private auth: AuthenticationService,
    private generator: GeneratorService,
    private notification: NzNotificationService,
    private apiCategorias: CategoriasService
  ) { 
    this.currentUser = this.auth.currentUserValue.user
  }

  ngOnInit(): void {
    this.getCategorias()
  }
  getCategorias() {
    this.isLoading = true
    const getCategoriasList = (ob: any) =>
    this.apiCategorias.getAll(this.params)
    .pipe(map((res: any) => res.data))
    .pipe(
      map((list: any) => {
        return list.map((item: any) => item)
      })
    );
    const dataTable$: Observable<any[]> = this.searchCategorias$
    .asObservable()
    .pipe(debounceTime(500))
    .pipe(switchMap(getCategoriasList));
    dataTable$.subscribe(data => {
      this.dataTable = data
      this.isLoading = false
      this.createNotification('success', 'Notificación', 'Se han cargado los registros exitosamente!')
    })
  }

  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
  handleCancel() {
    let categoria = {
      ...this.model,
      user_created_id: this.currentUser.id,
      user_updated_id: this.currentUser.id
    }
    this.isVisibleModal = false
  }
  handleOk() {
    let categoria = {
      ...this.model,
      user_created_id: this.currentUser.id,
      user_updated_id: this.currentUser.id
    }
    if (this.modalTitle == "Agregar categoría") {
      this.addCategoria(categoria)
    } else {

    }
  }
  addCategoria(model:any) {
    this.apiCategorias.post(model).subscribe((res:any) => {
      this.isVisibleModal = false
      this.getCategorias()
    })
  }
  delete(id:number) {
    this.apiCategorias.delete(id).subscribe((res:any) => {
      this.getCategorias()
    })
  }
  showModalAddEdit(section:string, data?:any) {
    if (section == "add") {
      this.model = {}
      this.modalTitle = "Agregar categoría"
    } else {
      this.model = {
        ...data
      }
      this.modalTitle = "Editar categoría"
    }
    this.isVisibleModal = true
  }
  downloadPdf() {
    let content = this.pdfExcelModel()
    let pdf = {
      header: "REPUESTOS | GRUPO SUDAMÉRICA",
      pageSize: 'A4',
      fontSize: 10,
      width: "*",
      orientation: "portrait",
      content: content
    }
    this.generator.generatePDF(pdf)
  }
  downloadExcel() {
    let content = this.pdfExcelModel()
    this.generator.exportExcel(content, "Registro categorías")
    this.export = false
  }
  pdfExcelModel() {
    let model = [['Item', 'Nombre', 'Descripción']]
    let n = 0
    this.dataTable.map((item:any) => {
      n += 1
      model.push([n, item.name, item.description])
    })
    return model
  }
  filterBy(key:string, value:any) {
    value = value.target.value.toString().toUpperCase()
    let params = {
      dataFilter: {
        [key]: value
      }
    }
    this.apiCategorias.getAll(params).subscribe((res:any) => {
      this.dataTable = res['data']
    })
  }
  visibleFilter() {
    this.showFilter = !this.showFilter
  }
}
