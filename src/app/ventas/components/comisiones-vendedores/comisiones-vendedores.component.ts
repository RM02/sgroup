import { Component, OnInit } from '@angular/core';
import { ComisionesVendedoresService, AuthenticationService, SellersService, GeneratorService } from '../../../core/services/index';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-comisiones-vendedores',
  templateUrl: './comisiones-vendedores.component.html',
  styleUrls: ['./comisiones-vendedores.component.scss']
})
export class ComisionesVendedoresComponent implements OnInit {

  dataTable: any = [];
  titleModal: string = '';
  isLoading: boolean = false;
  export: boolean = false;
  amount: any = 0;
  seller_id: number = 0;
  seller_name: string = '';
  
  comisionTypes: string[] = ['Monto', 'Porcentaje']
  selectedSeller: any = [];
  sellerList: any = [];
  commission_type: string = '';
  isVisibleModal: boolean = false;
  selectedComision: any = [];
  currentUser: any;
  params:any = {};

  constructor(
    private notification: NzNotificationService,
    private auth: AuthenticationService,
    private generator: GeneratorService,
    private apiSeller: SellersService,
    private apiComisiones: ComisionesVendedoresService
  ) {
    this.currentUser = this.auth.currentUserValue.user
   }

  ngOnInit(): void {
    this.getComisiones()
    this.getSellers()
  }
  getSellers() {
    this.apiSeller.getAll(this.params).subscribe((res:any) => {
      this.sellerList = res['data']
    })
  }
  getComisiones() {
    this.isLoading = true
    this.apiComisiones.getAll().subscribe((res:any) => {
      this.dataTable = res['data']
      this.isLoading = false
      this.createNotification('success', 'Notificación', 'Se han cargado los registros exitosamente!')
    })
  }
  onSearch(value: any) {
    this.params = {
      dataSearch: {
        name: value.toUpperCase()
      }
    }
    debounceTime(100)
    this.getSellers()
  }
  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
  
  delete(id:number) {
    this.apiComisiones.delete(id).subscribe((res) => {
      this.getComisiones()
    })
  }
  showAddModal() {
    this.titleModal = 'Agregar nueva comisión'
    this.commission_type = '',
    this.amount = '',
    this.selectedComision = ''
    this.isVisibleModal = true
  }
  showModal (data:any) {
    this.seller_id = data.seller_id
    this.seller_name = data.seller.name
    this.amount = data.amount
    this.selectedComision = data.commission_type
    this.titleModal = 'Editar comisión'
    this.isVisibleModal = true
  }
  handleCancel() {
    this.isVisibleModal = false
  }
  handleOk() {
      let model = {
        seller_id: this.seller_id,
        commission_type: this.selectedComision,
        amount: this.amount,
        user_created_id: this.currentUser.id,
        user_updated_id: this.currentUser.id
      }
    if (this.titleModal == 'Editar comisión') {

      this.apiComisiones.update(model.seller_id, model).subscribe((res) => {
        this.isVisibleModal = false
        this.getComisiones()
      })
    }
    else if (this.titleModal == 'Agregar nueva comisión') {
      model['seller_id'] = this.selectedSeller
      this.apiComisiones.post(model).subscribe((res) => {
        this.isVisibleModal = false
        this.getComisiones()
      })
    }
  }
  
  onSelectedComision(value:any) {
    console.log(value)
  }
  
  downloadPdf() {
    let content = this.pdfExcelModel()
    let pdf = {
      header: "REPUESTOS | GRUPO SUDAMÉRICA",
      pageSize: 'A4',
      fontSize: 10,
      width: "*",
      orientation: "portrait",
      content: content
    }
    this.generator.generatePDF(pdf)
  }
  downloadExcel() {
    let content = this.pdfExcelModel()
    this.generator.exportExcel(content, "Registro comisiones vendedores")
    this.export = false
  }
  pdfExcelModel() {
    let model = [['Item', 'Vendedor', 'Tipo comisión', 'Comisión']]
    let n = 0
    this.dataTable.map((item:any) => {
      n += 1
      model.push([n, `${item.seller.name} ${item.seller.last_name}`, item.commission_type, item.amount])
    })
    return model
  }
}
