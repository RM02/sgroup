import { Component, OnInit } from '@angular/core';
import { PedidoService, GeneratorService } from '../../../core/services/index';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.scss']
})
export class PedidosComponent implements OnInit {
  
  dataTable: any = [];
  details: any = [];
  isVisibleDetails: boolean = false;
  showFilter: boolean = false;
  export: boolean = false;
  isLoading: boolean = false;

  constructor(
    private generator: GeneratorService,
    private notification: NzNotificationService,
    private apiPedido: PedidoService
  ) { }

  ngOnInit(): void {
    this.getPedidos()
  }

  getPedidos() {
    this.isLoading = true
    this.apiPedido.getAll().subscribe((res:any) => {
      this.dataTable = res['data']
      this.isLoading = false
      this.createNotification('success', 'Notificación', 'Se han cargado los registros exitosamente!')
    })
  }
  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
  edit(data:any) {

  }
  delete(id:number) {
    this.apiPedido.delete(id).subscribe((res:any) => {
      this.getPedidos()
    })
  }
  showDetails(data:any) {
    this.details['coin'] = data.coin.name
    this.details = data.order_details
    this.isVisibleDetails = true
  }
  handleCancelDetails() {
    this.isVisibleDetails = false
  }
  handleOkDetails() {
    this.isVisibleDetails = false
  }
  
  downloadPdf() {
    let content = this.pdfExcelModel()
    let pdf = {
      header: "REPUESTOS | GRUPO SUDAMÉRICA",
      pageSize: 'A4',
      fontSize: 12,
      width: "*",
      orientation: "portrait",
      content: content
    }
    this.generator.generatePDF(pdf)
  }
  downloadExcel() {
    let content = this.pdfExcelModel()
    this.generator.exportExcel(content, "Registro pedidos")
    this.export = false
  }
  pdfExcelModel() {
    let model = [['Item', 'Fecha de entrega', 'Nombre/Razón social', 'Dirección']]
    let n = 0
    this.dataTable.map((item:any) => {
      n += 1
      model.push([n, item.delivery_date, item.client.name, item.address])
    })
    return model
  }
  filterBy(key:string, value:any) {
    value = value.target.value.toString().toUpperCase()
    let params = {
      dataSearch: {
        [key]: value
      }
    }
    this.apiPedido.getAll(params).subscribe((res:any) => {
      this.dataTable = res
    })
  }
  visibleFilter() {
    this.showFilter = !this.showFilter
  }
}
