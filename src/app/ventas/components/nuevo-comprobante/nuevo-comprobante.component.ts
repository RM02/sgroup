import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime, map, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';

import { 
  SellersService,
  ClientService,
  CoinService,
  VoucherTypeService,
  OperationTypesService,
  ExternalService,
  PaymentService,
  ProductoService,
  AuthenticationService,
  SeriesService,
  ClientTypeService,
  DocumentTypeService,
  GeneratorService
} from '../../../core/services/index';
import { User } from 'src/app/core/model/users';

@Component({
  selector: 'app-nuevo-comprobante',
  templateUrl: './nuevo-comprobante.component.html',
  styleUrls: ['./nuevo-comprobante.component.scss']
})
export class NuevoComprobanteComponent implements OnInit {
  
  searchChangeOperation$ = new BehaviorSubject('');
  searchChangeVoucherType$ = new BehaviorSubject('');

  searchCoin$ = new BehaviorSubject('');
  searchClient$ = new BehaviorSubject('');
  searchSeller$ = new BehaviorSubject('');
  searchPayment$ = new BehaviorSubject('');
  searchProduct$ = new BehaviorSubject('');
  searchDestination$ = new BehaviorSubject('');

  model: any = {
    concept: 'Venta',
    expiration_date: new Date(),
  };
  productDetails: any = {
    igv: "",
    price: "",
    amount: "",
    total: ""
  };

  searchLabel: string = "RENIEC";
  isVisibleMiddle: boolean = false;
  isVisibleModal: boolean = false;
  showEdit: boolean = false;
  selectedDocument: any = {};
  selectedClienteType: any = {};
  clientModel: any = {};
  documentTypeList: any = [];
  clienteTypeList: any = [];
  noRegister: boolean = false;

  isConfirmed = false;
  params: any = {};
  optionList: string[] = [];
  paymentDestinationList: any = [];
  productList: any = [];
  paymentMethodList: any = [];

  selectedUser?: any;
  selectedSeller?: any;
  selectedProduct?: any;
  selectedOperation?: any = {};
  selectedVoucher?: any;
  selectedCoin?: any;
  selectedPayment?: any;
  selectedDestination?: any;
  selectedPaymentDestination?: any;
  total: number = 0;
  currentCoin: string = 'S/';
  date = new Date();
  expired_date = null;
  exchange_rate: number = 0;
  dataTable: any = [];
  operationTypeList: any = [];
  sellerList: any = [];
  coinList: any = [];
  clientList: any = [];
  voucherTypeList: any = [];

  isLoading = false;
  serie_id: number = 0;
  currentUser: User;

  constructor(
    private apiSerie: SeriesService,
    private apiVoucher: VoucherTypeService,
    private apiCoin: CoinService,
    private router: Router,
    private generator: GeneratorService,
    private apiDocumentType: DocumentTypeService,
    private apiClienteType: ClientTypeService,
    private apiExternal: ExternalService,
    private auth: AuthenticationService,
    private apiSeller: SellersService,
    private apiClient: ClientService,
    private apiPayment: PaymentService,
    private apiProductos: ProductoService,
    private apiOperationType: OperationTypesService,
  ) {
    this.currentUser = this.auth.currentUserValue.user
  }

  ngOnInit() {
    this.getStore()
    this.getOperationType()
    this.getVoucherType()
    this.getCoin()
    this.getClient()
    this.getSeller()
    this.getPaymentMethods()
    this.getProducto()
    this.getPaymentDestination()
    this.getExchange(this.date)
    this.getSerie()
    this.getClienteType()
    this.getDocumentType()
  }
  getStore() {
    let selectedProducts = JSON.parse(localStorage.getItem('products-ventas') || '{}') 
    if (selectedProducts.data) {
      this.dataTable = selectedProducts['data'].map((item:any) => {
        let product = {
          product_id: item.id,
          description: item.description,
          price: item.stock[0].sale_price,
          purchase_price: item.stock[0].purchase_price,
          igv: 12,
          total: item.stock[0].sale_price * item.amount,
          amount: item.amount
        }
        this.total += product.total
        return product
      })
    }
  }
  getSerie() {
    this.apiSerie.getAll().subscribe((res:any) => {
      this.serie_id = res['data'][1].id
    })
  }
  onChange(result: Date): void {
    console.log('onChange: ', result);
  }
  onSearchProduct(value: string): void {
    this.params = {
      dataSearch: {
        name: value
      }
    }
    this.searchProduct$.next(value);
  }
  onDestinationSearch(value: string): void {
    this.params = {
      dataSearch: {
        name: value
      }
    }
    this.searchDestination$.next(value);
  }
  getOperationType() {
    const getOperationList = (name: string) =>
    this.apiOperationType.getAll()
    .pipe(map((res: any) => res.data))
    .pipe(
      map((list: any) => {
        return list.map((item: any) => item)
      })
    );
    const operationTypeList$: Observable<string[]> = this.searchChangeOperation$
    .asObservable()
    .pipe(debounceTime(500))
    .pipe(switchMap(getOperationList));
    operationTypeList$.subscribe(data => {
      this.operationTypeList = data
      this.selectedOperation = data[1]
    })
  }
  getVoucherType() {
    const getVoucherList = (name: string) =>
    this.apiVoucher.getAllVouchers()
    .pipe(map((res: any) => res.data))
    .pipe(
      map((list: any) => {
        return list.map((item: any) => item)
      })
    );
    const voucherTypeList$: Observable<string[]> = this.searchChangeVoucherType$
    .asObservable()
    .pipe(debounceTime(500))
    .pipe(switchMap(getVoucherList));
    voucherTypeList$.subscribe(data => {
      this.voucherTypeList = data
    })
  }
  getCoin() {
    const getCoinList = (name: string) =>
    this.apiCoin.getAll()
    .pipe(map((res: any) => res.data))
    .pipe(
      map((list: any) => {
        return list.map((item: any) => item)
      })
    );
    const coinList$: Observable<string[]> = this.searchCoin$
    .asObservable()
    .pipe(debounceTime(500))
    .pipe(switchMap(getCoinList));
    coinList$.subscribe(data => {
      this.model['coin'] = data[0]
      this.coinList = data
    }) 
  }
  getSeller() {
    const getSellerList = (name: string) =>
    this.apiSeller.getAll(this.params)
    .pipe(map((res: any) => res.data))
    .pipe(
      map((list: any) => {
        return list.map((item: any) => item)
      })
    );
    const sellerList$: Observable<string[]> = this.searchSeller$
    .asObservable()
    .pipe(debounceTime(500))
    .pipe(switchMap(getSellerList));
    sellerList$.subscribe(data => {
      this.sellerList = data
    }) 
  }
  getClient() {
    this.apiClient.getAll(this.params)
    .subscribe((res) => {
      this.clientList = res
    })
  }
  onSearchCliente(value:any) {
    this.params = {
      dataSearch: {
        name: value.toUpperCase()
      }
    }
    debounceTime(100)
    this.getClient()
  }
  getProducto() {
    const getProductoList = (name: string) =>
    this.apiProductos.getProducts(this.params)
    .pipe(map((res: any) => res))
    .pipe(
      map((list: any) => {
        return list.map((item: any) => item)
      })
    );
    const productList$: Observable<string[]> = this.searchProduct$
    .asObservable()
    .pipe(debounceTime(500))
    .pipe(switchMap(getProductoList));
    productList$.subscribe(data => {
      this.productList = data
    })
  }
  getExchange(date:Date) {
    this.model['created_at'] = date
    this.getExchangeValues(date)
  }
  getExchangeValues (date:any) {
    this.apiExternal.getExchangeValue(date).subscribe((res: any) => {
      
      if (res['exchange_rates'].length > 0 && res['exchange_rates']) {
        let i = res['exchange_rates'].length - 1
        this.exchange_rate = res['exchange_rates'][i]['venta']
      }
    })
  }
  getPaymentMethods() {
    const getPaymentMethodList = (name: string) =>
    this.apiPayment.getAll(this.params)
    .pipe(map((res: any) => res.data))
    .pipe(
      map((list: any) => {
        return list.map((item: any) => item)
      })
    );
    const paymentMethodList$: Observable<string[]> = this.searchPayment$
    .asObservable()
    .pipe(debounceTime(500))
    .pipe(switchMap(getPaymentMethodList));
    paymentMethodList$.subscribe(data => {
      this.paymentMethodList = data
    }) 
  }
  getPaymentDestination() {
    const getPaymentDestinationList = (name: string) =>
    this.apiPayment.getDestinations()
    .pipe(map((res: any) => res.data))
    .pipe(
      map((list: any) => {
        return list.map((item: any) => item)
      })
    );
    const paymentDestinationList$: Observable<string[]> = this.searchDestination$
    .asObservable()
    .pipe(debounceTime(500))
    .pipe(switchMap(getPaymentDestinationList));
    paymentDestinationList$.subscribe(data => {
      this.paymentDestinationList = data
    }) 
  }
  showModalMiddle():void {
    this.isVisibleMiddle = true;
  }
  handleOkMiddle(): void {
    let product = {
      product_id: this.selectedProduct.id,
      description: this.selectedProduct.name,
      amount: this.productDetails.amount.target.value,
      igv: 12,
      purchase_price: parseFloat(this.productDetails.price.target.value),
      price: parseFloat(this.productDetails.price.target.value),
      total: parseFloat(this.productDetails.amount.target.value)*parseFloat(this.productDetails.price.target.value)
    }
    this.total += product['total']
    this.currentCoin = this.model['coin'] ? this.model['coin'].symbol : 'S/'
    this.dataTable = [
      ...this.dataTable,
      product
    ]
    this.isVisibleMiddle = false;
  }
  handleCancelMiddle(): void {
    this.isVisibleMiddle = false;
  }
  deleteProduct(index:number) {
    this.dataTable.splice(index, 1)
    this.dataTable = [
     ...this.dataTable
    ]
  }
  handleCancelConfirm () {
    this.isConfirmed = false
  }
  handleOkConfirm () {
    this.isConfirmed = false
  }
  navigateTo(route:string) {
    this.router.navigate([route])
  }
  save() {
    let comprobante = {
      client_id: this.selectedUser.id,
      voucher_type_id: this.model['voucher_type_id'],
      branch_office_id: this.currentUser.branch_offices[0].id,
      operation_type_id: this.selectedOperation.id,
      seller_id: this.model['seller_id'],
      coin_id: this.model['coin'].id,
      serie_id: this.serie_id,
      igv: "12",
      exchange_rate: this.exchange_rate,
      expiration_date: `${this.model['expiration_date'].getFullYear()}-${this.model['expiration_date'].getMonth()}-${this.model['expiration_date'].getDay()}`,
      bill_electronic_details: this.dataTable.map((item:any) => {
        return {
          product_id: item.product_id,
          amount: item.amount,
          discount: 0,
          price: item.price,
          igv: item.igv,
          purchase_price: item.price,
          user_created_id: this.currentUser.id
        }
      }),
      bill_electronic_payments: [
        {
          payment_method_id: this.model['payment_method_id'],
          payment_destination_id: this.model['payment_destination_id'],
          reference: 'N/A',
          amount: this.total,
          user_created_id: this.currentUser.id
        }
      ],
      bill_electronic_guides: [],
      user_created_id: this.currentUser.id,
      user_updated_id: this.currentUser.id,
      created_at: this.model['created_at']
    }
    this.apiVoucher.post(comprobante).subscribe((res) => {
      this.isConfirmed = true
      localStorage.setItem('products-ventas', '{}')
    })
  }
  handleAddClientCancel() {
    this.isVisibleModal = false
  }
  handleAddClientOk() {
    this.clientModel['last_name'] = this.selectedDocument.name == 'RUC' ? 'N/A' : this.clientModel['last_name']
    this.clientModel['full_name_contact'] = this.clientModel['name']
    this.clientModel['phone_contact'] = this.clientModel['phone']

    let model = {
      ...this.clientModel,
      document_type_id: this.selectedDocument.id,
      client_type_id: this.selectedClienteType.id,
      user_created_id: this.currentUser.id,
      user_updated_id: this.currentUser.id
    }
    this.apiClient.postClient(model).subscribe((res:any) => {
      this.getClient()
      this.isVisibleModal = false
    })
  }

  searchDniRuc() {
    if (this.selectedDocument.name == 'RUC') {
      this.apiExternal.getRuc(this.clientModel['document_number']).subscribe((res:any) => {
        if (res.success) {
          this.noRegister = false
          this.clientModel['name'] = res['nombre_o_razon_social']
        } else {
          this.noRegister = true
        }
      })
    }
    else if (this.selectedDocument.name == 'DNI') {
      // this.showLabelLastName = true
      this.apiExternal.getData(this.clientModel['document_number']).subscribe((client:any) => {
        if (client['name']) {
          this.clientModel['name'] = client['name']
          this.clientModel['last_name'] = `${client['first_name']} ${client['last_name']}`
        } else {
          this.noRegister = true
        }
      })
    }
  }
  onSelectedDocument(value?:any) {
    this.selectedDocument = value
    if(this.selectedDocument && this.selectedDocument.name == 'RUC') {
      this.searchLabel = 'SUNAT'
    }
    else if (this.selectedDocument && this.selectedDocument.name == 'DNI') {
      this.searchLabel = 'RENIEC'
    }
  }
  
  getClienteType() {
    this.apiClienteType.getClientTypes().subscribe((res:any) => {
      this.clienteTypeList = res['data']
    })
  }
  getDocumentType() {
    this.apiDocumentType.getAll().subscribe((res:any) => {
      this.selectedDocument = res['data'][1]
      this.documentTypeList = res['data']
    })
  }
  generarComprobante() {
    let client = this.selectedUser.fullName
    let client_number = this.selectedUser.document_number
    let content = [['Cantidad', 'Descripción', 'P. Unitario', 'Importe']]
    this.dataTable.map((item:any) => {
      content.push([item.amount, item.description, item.price, item.price])
    })
    let data = {
      pageSize: "A4",
      fontSize: 12,
      date: this.date.toLocaleString(),
      number: client_number,
      moneda: this.currentCoin,
      client: client,
      total: this.total,
      content: content
    }
    this.generator.generateComprobante(data)
  }
}
