import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevaGuiaComponent } from './nueva-guia.component';

describe('NuevaGuiaComponent', () => {
  let component: NuevaGuiaComponent;
  let fixture: ComponentFixture<NuevaGuiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuevaGuiaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevaGuiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
