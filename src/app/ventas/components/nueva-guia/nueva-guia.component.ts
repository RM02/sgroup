import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  ComprobanteService,
  AuthenticationService,
  ClientService,
  ProductoService
} from '../../../core/services/index';

@Component({
  selector: 'app-nueva-guia',
  templateUrl: './nueva-guia.component.html',
  styleUrls: ['./nueva-guia.component.scss']
})
export class NuevaGuiaComponent implements OnInit {

	dataTable: any = [];
	clientList: any = [];

	params: any = [];
	model: any = {};
	currentUser: any = {};

  constructor(
  	private apiComprobante: ComprobanteService,
  	private apiClient: ClientService,
  	private route: ActivatedRoute,
  	private apiProduct: ProductoService,
  	private auth: AuthenticationService
  	) {
  		this.currentUser = this.auth.currentUserValue.user
  	 }

  ngOnInit(): void {
    let id = parseInt(this.route.snapshot.paramMap.get('id') || '0', 10)
  	this.getOneComprobante(id)
  }
  getOneComprobante(id:number) {
  	this.apiComprobante.getOne(id).subscribe((data:any) => {
  		this.model = data
  		this.model['client'] = this.getOneClient(data.client_id)
  		this.dataTable = this.model.details.map((item:any) => {
  			return this.getOneProduct(item.id)
  		})
  	})
  }
  getOneClient(id:number) {
  	let client = null
  	this.apiClient.getOne(id).subscribe((res:any) => {
  		client = res
  	})
  	return client
  }
  getOneProduct(id:string) {
  	let product = null
  	this.apiProduct.getProductById(id).subscribe((res:any) => {
  		product = res
  	})
  	return product
  }

}
