import { Component, OnInit } from '@angular/core';
import { catchError } from 'rxjs/operators';

import { 
  ClientService,
  ClientTypeService,
  DocumentTypeService,
  ExternalService,
  AuthenticationService,
  GeneratorService
} from '../../../core/services/index';
import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime, map, switchMap } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { User } from '../../../core/model/users';
import { ClienteModel } from '../../../core/model/clientes.interface';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {

  searchClientes$ = new BehaviorSubject('');

  searchLabel: string = "RENIEC";
  modalMode: string = '';
  searchPlaceholder: string = "";

  isConfirmed: boolean = false;
  showFilter: boolean = false;
  isLoading: boolean = false;
  noRegister: boolean = false;
  showLabelLastName: boolean = false;
  isVisibleModal: boolean = false;

  dataTable: any = [];
  model: any = {};
  title: string = "";
  clienteTypeList: any = [];
  selectedDocument: any = {};
  selectedClienteType: any = {};
  documentTypeList: any = [];
  tplModalButtonLoading = false;
  disabled = false;
  params: any = {};
  currentUser: User;

  constructor(
    private notification: NzNotificationService,
    private apiCliente: ClientService,
    private generator: GeneratorService,
    private apiExternal: ExternalService,
    private apiDocumentType: DocumentTypeService,
    private apiClienteType: ClientTypeService,
    private auth: AuthenticationService
  ) {
    this.currentUser = this.auth.currentUserValue['user']
    this.searchLabel = this.selectedDocument.name == 'DNI' ? 'RENIEC': 'SUNAT'
   }

  ngOnInit(): void {
    this.getClientes()
    this.getClienteType()
    this.getDocumentType()
  }
  
  getClientes() {
    this.isLoading = true
    const getClienteList = (ob: any) =>
    this.apiCliente.getAll(this.params)
    .pipe(map((res: any) => res))
    .pipe(
      map((list: any) => {
        return list.map((item: any) => item)
      })
    );
    const dataTable$: Observable<any[]> = this.searchClientes$
    .asObservable()
    .pipe(debounceTime(500))
    .pipe(switchMap(getClienteList));
    dataTable$.subscribe(data => {
      this.dataTable = data
      this.isLoading = false
      this.createNotification('success', 'Notificación', 'Se han cargado los registros exitosamente!')
    })
  }
  getClienteType() {
    this.apiClienteType.getClientTypes().subscribe((res:any) => {
      this.selectedClienteType = res['data'][1]
      this.clienteTypeList = res['data']
    })
  }
  getDocumentType() {
    this.apiDocumentType.getAll().subscribe((res:any) => {
      this.selectedDocument = res['data'][0]
      this.documentTypeList = res['data']
    })
  }
  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
  handleOk() {
    if (this.modalMode == 'add') {
      this.model['last_name'] = this.selectedDocument.name == 'RUC' ? 'N/A' : this.model['last_name']
      this.model['full_name_contact'] = this.model['name']
      this.model['phone'] = this.model['phone']
      this.model['phone_contact'] = this.model['phone']
      this.model['email'] = this.model['email']
      let model = {
        ...this.model,
        document_type_id: this.selectedDocument.id,
        client_type_id: this.selectedClienteType ? this.selectedClienteType.id : 1,
        user_created_id: this.currentUser.id,
        user_updated_id: this.currentUser.id
      }
      this.apiCliente.postClient(model)
      .subscribe(
        res => {
          this.isVisibleModal = false
          this.getClientes()
        },
        err => {
          if (err.status == 500) {
            this.createNotification('error', 'Error', 'Por favor, complete todos los campos!')
          }
        })

    } else if (this.modalMode == 'edit') {
      let client = {
        ...this.model,
        document_type_id: this.selectedDocument.id,
        client_type_id: this.selectedClienteType.id,
        user_created_id: this.currentUser.id
      }
      this.apiCliente.updateClient(client.id, client).subscribe(() => {
        this.isVisibleModal = false
        this.getClientes()
      })
    }

  }
  handleCancel() {
    this.isVisibleModal = false
  }
  delete(id:number) {
    this.apiCliente.delete(id).subscribe((res:any)=> {
      this.getClientes()
    })
  }
  showAdd() {
    this.modalMode = 'add'
    this.title = "Agregar cliente"
    this.model = {...{}}
    this.noRegister = false
    this.isVisibleModal = true
  }
  onSelectedDocument(value?:any) {
    this.selectedDocument = value
    if(this.selectedDocument && this.selectedDocument.name == 'RUC') {
      this.showLabelLastName = false
      this.searchPlaceholder = "0/11"
      this.searchLabel = 'SUNAT'
    }
    else if (this.selectedDocument && this.selectedDocument.name == 'DNI') {
      this.showLabelLastName = true
      this.searchPlaceholder = "0/8"
      this.searchLabel = 'RENIEC'
    }
  }
  searchDniRuc() {
    if (this.selectedDocument.name == 'RUC') {
      this.apiExternal.getRuc(this.model['document_number']).subscribe((res:any) => {
        if (res.success) {
          this.noRegister = false
          this.model['name'] = res['nombre_o_razon_social']
        } else {
          this.noRegister = true
        }
      })
    }
    else if (this.selectedDocument.name == 'DNI') {
      this.showLabelLastName = true
      this.apiExternal.getData(this.model['document_number']).subscribe((client:any) => {
        if (client['name']) {
          this.model['name'] = client['name']
          this.model['last_name'] = `${client['first_name']} ${client['last_name']}`
        } else {
          this.noRegister = true
        }
      })
    }
  }
  showEdit(data: ClienteModel) {
    this.noRegister = false
    this.title = "Editar cliente"
    this.modalMode = 'edit'
    // this.onSelectedDocument()
    this.model = {
      ...data
    }
    this.isVisibleModal = true
  }
  onSelectClientType(value:any) {

  }
  downloadExcel() {
    let data = this.createExcelForm(this.dataTable)
    this.generator.exportExcel(data, "Registro de clientes")
    this.isConfirmed = false
  }
  downloadPDF() {
    let pdfContent = this.createExcelForm(this.dataTable)
    let data = {
      header: "REPUESTOS | GRUPO SUDAMÉRICA",
      content: pdfContent,
      title: 'Registro de clientes'
    }
    this.generator.generatePDF(data)
  }

  createExcelForm(data:any, type_doc?: string) {

    var excelData = [['Item', 'Nombre/Razón social', 'N° R.U.C', 'Correo', 'Tipo cliente']]

    for (let index = 0; index < data.length; index++) {

        let element = [index + 1, data[index]['fullName'], data[index]['document_number'], data[index]['email'], data[index]['clientType']];
        excelData.push(element)
      }
      return excelData
    }
    visibleFilter() {
      this.showFilter = !this.showFilter
    }
    filterBy(key:string, value:any) {
      value = value.target.value.toString().toUpperCase()
      this.params = {
        dataSearch: {
          [key]: value
        }
      }
      this.apiCliente.getAll(this.params).subscribe((res:any) => {
        this.dataTable = res
      })
    }
}
