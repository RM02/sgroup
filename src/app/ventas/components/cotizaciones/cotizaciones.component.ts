import { Component, OnInit } from '@angular/core';
import { AuthenticationService, CotizacionesService, GeneratorService } from '../../../core/services/index';

@Component({
  selector: 'app-cotizaciones',
  templateUrl: './cotizaciones.component.html',
  styleUrls: ['./cotizaciones.component.scss']
})
export class CotizacionesComponent implements OnInit {
  
  isLoading: boolean = false;
  export: boolean = false;
  showFilter: boolean = false;
  isVisibleDetails: boolean = false;
  cotizacionesDetails: any = [];
  dataTable: any = [];
  params: any = {};
  currentUser: any = {};

  constructor(
    private apiCotizaciones: CotizacionesService,
    private generator: GeneratorService,
    private auth: AuthenticationService
  ) {
    this.currentUser = this.auth.currentUserValue.user
   }

  ngOnInit(): void {
    this.getCotizaciones()
  }
  getCotizaciones() {
    this.isLoading = true
    this.apiCotizaciones.getAll(this.params).subscribe((res:any) => {
      res['data'].map((item:any) => {
        item.created_at = new Date(item.created_at).toLocaleString()
      })
      this.dataTable = res['data']
      this.isLoading = false
    })
  }
  handleCancelDetails() {
    this.isVisibleDetails = false
  }
  showDetails(data:any) {
    this.cotizacionesDetails = data.quotation_details
    this.isVisibleDetails = true
  }
  handleOkDetails (){
    this.isVisibleDetails = false
  }
  delete(id:number) {
    this.apiCotizaciones.delete(id).subscribe((res:any) => {
      this.getCotizaciones()
    })
  }
  
  downloadPdf() {
    let content = this.pdfExcelModel()
    let pdf = {
      header: "REPUESTOS | GRUPO SUDAMÉRICA",
      pageSize: 'A4',
      fontSize: 13,
      width: "*",
      orientation: "portrait",
      content: content
    }
    this.generator.generatePDF(pdf)
  }
  downloadExcel() {
    let content = this.pdfExcelModel()
    this.generator.exportExcel(content, "Registro cotizaciones")
    this.export = false
  }
  pdfExcelModel() {
    let model = [['Item', 'Fecha emisión', 'Nombre/Razón social']]
    let n = 0
    this.dataTable.map((item:any) => {
      n += 1
      model.push([n, item.created_at, item.client.name])
    })
    return model
  }
  visibleFilter() {
    this.showFilter = !this.showFilter
  }
  filterBy(key:string, value:any) {
    value = value.target.value.toString()
    let params = {
      dataSearch: {
        [key]: value
      }
    }
    this.apiCotizaciones.getAll(params).subscribe((res:any) => {
      this.dataTable = res['data']
    })
  }
}
