import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NuevoComprobanteComponent } from './components/nuevo-comprobante/nuevo-comprobante.component';
import { ListaComprobantesComponent } from './components/lista-comprobantes/lista-comprobantes.component';
import { VentasComponent } from './ventas.component';
import { ProductosComponent } from './components/productos/productos.component';
import { MarcasComponent } from './components/marcas/marcas.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { CategoriasComponent } from './components/categorias/categorias.component';
import { PedidosComponent } from './components/pedidos/pedidos.component';
import { CotizacionesComponent } from './components/cotizaciones/cotizaciones.component';
import { ComisionesVendedoresComponent } from './components/comisiones-vendedores/comisiones-vendedores.component';
import { ComisionesProductosComponent } from './components/comisiones-productos/comisiones-productos.component';
import { NuevoPedidoComponent } from './components/nuevo-pedido/nuevo-pedido.component';
import { NuevaCotizacionComponent } from './components/nueva-cotizacion/nueva-cotizacion.component';
import { NotaCreditoComponent } from './components/nota-credito/nota-credito.component';
import { SharedComponent } from './components/shared/shared.component';
import { NuevaGuiaComponent } from './components/nueva-guia/nueva-guia.component';

const routes: Routes = [
    { 
        path: '',
        component: VentasComponent,
        children: [
            { path: 'listado-comprobante', component: ListaComprobantesComponent },
            { path: 'nuevo-comprobante', component: NuevoComprobanteComponent },
            { path: 'catalogos/productos', component: ProductosComponent },
            { path: 'catalogos/marcas', component: MarcasComponent },
            { path: 'catalogos/clientes', component: ClientesComponent },
            { path: 'catalogos/categorias', component: CategoriasComponent },
            { path: 'pedidos', component: PedidosComponent },
            { path: 'cotizaciones', component: CotizacionesComponent },
            { path: 'comisiones/vendedores', component: ComisionesVendedoresComponent },
            { path: 'comisiones/productos', component: ComisionesProductosComponent },
            { path: 'nuevo-pedido', component: NuevoPedidoComponent },
            { path: 'nueva-cotizacion', component: NuevaCotizacionComponent },
            { path: 'nota-credito/:id', component: NotaCreditoComponent },
            { path: 'nueva-guia/:id', component: NuevaGuiaComponent },
            { path: 'agregar-productos', component: SharedComponent }
        ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VentasRoutingModule { }
